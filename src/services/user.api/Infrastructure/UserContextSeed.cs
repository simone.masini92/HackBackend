using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Retry;
using user.api.Models.Entities;

namespace user.api.Infrastructure
{
    public class UserContextSeed
    {
        private readonly UserManager<User> _userManager;

        public UserContextSeed(UserManager<User> userManager)
        {
            _userManager = userManager;
        }
        public async Task SeedAsync(UserContext context, IHostingEnvironment env, ILogger<UserContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(UserContextSeed));

            await policy.ExecuteAsync(async () =>
            {
                var contentRootPath = env.ContentRootPath;

//                if (!context.RolesBackoffice.Any())
//                {
//                    await context.RolesBackoffice.AddRangeAsync(RoleBackofficeSeed.GetFromFile(contentRootPath, logger));
//                    SetIdentityInsertOn(context, "RoleBackoffice");
//                    await context.SaveChangesAsync();
//                }
//
//                if (!context.PermissionsBackoffice.Any())
//                {
//                    await context.PermissionsBackoffice.AddRangeAsync(PermissionBackofficeSeed.GetFromFile(contentRootPath, logger));
//                    SetIdentityInsertOn(context, "PermissionBackoffice");
//                    await context.SaveChangesAsync();
//                }
//
//                if (!context.RolePermissionsBackoffice.Any())
//                {
//                    await context.RolePermissionsBackoffice.AddRangeAsync(RolePermissionBackofficeSeed.GetFromFile(contentRootPath, logger));
//                    await context.SaveChangesAsync();
//                }
//
//                if (!context.Users.Any())
//                {
//                    var userResults = UserSeed.GetFromFile(contentRootPath, logger);
//                    foreach (var (user, password) in userResults)
//                    {
//                        var result = await _userManager.CreateAsync(user, password);
//                        result.ThrowIdentityError();
//                    }
//                }
            });
        }
        
        private static AsyncRetryPolicy CreatePolicy(ILogger logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>()
                .WaitAndRetryAsync(
                    retries,
                    retry => TimeSpan.FromSeconds(5),
                    (exception, timeSpan, retry, ctx) =>
                    {
                        logger.LogTrace($"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
                    }
                );
        }

        private static void SetIdentityInsertOn(DbContext context, string table)
        {
            context.Database.OpenConnection();
            try
            {
                context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo." + table + " ON");
                context.SaveChanges();
            }
            finally
            {
                context.Database.CloseConnection();
            }
        }
    }
}