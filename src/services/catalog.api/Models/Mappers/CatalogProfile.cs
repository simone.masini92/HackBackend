using AutoMapper;
using catalog.api.Models.Dto.Request;
using catalog.api.Models.Dto.Response;
using catalog.api.Models.Entities;

namespace catalog.api.Models.Mappers
{
    public class CatalogProfile : Profile
    {
        public CatalogProfile()
        {
            CreateMap<CatalogItem, CatalogItemRequestDto>()
                .ReverseMap();
            
            CreateMap<CatalogItem, CatalogItemResponseDto>()
                .ReverseMap();
        }
    }
}