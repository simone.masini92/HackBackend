using System;

namespace setup.api.Models.Dto
{
    public class FileInfoDto
    {
        public string FilePath { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public decimal Size { get; set; }
        public int LinesNumber { get; set; }
    }
}