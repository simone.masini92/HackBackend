﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;
using System.Text;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Hangfire;
using Hangfire.PostgreSql;
using infrastructure.Filters;
using infrastructure.Repositories;
using infrastructure.Services;
using Microsoft.AspNetCore.Authentication;
using models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.Swagger;

namespace utilities.Extension
{
    public static class CustomServicesExtension
    {
        /// <summary>
        /// Aggiunge l'mvc con le Exception
        /// Viene ignorato il reference loop del db
        /// Viene aggiunta la policy per il cors
        /// </summary>
        public static IServiceCollection AddCustomMvc(this IServiceCollection services)
        {
            services.AddMvc(options =>
                {
                    options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                })
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore)
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("CorsPolicy"));
            });

            return services;
        }

        /// <summary>
        /// Viene specificato l'utilizzo di SqlServer con la retrocompatibilità per SqlServer 2008
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <param name="type">E' lo Startup</param>
        /// <typeparam name="TContext">Il Il DbContext del microservizio</typeparam>
        /// <returns></returns>
        public static IServiceCollection AddCustomDbContext<TContext>(
            this IServiceCollection services,
            IConfiguration configuration, 
            Type type) where TContext : DbContext
        {
            services.AddDbContext<TContext>(options =>
            {
                options.UseNpgsql(configuration["ConnectionString"],
                        sqlOptions =>
                        {
                            sqlOptions.MigrationsAssembly(type.GetTypeInfo().Assembly.GetName().Name);

                            //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                            sqlOptions.EnableRetryOnFailure(10, TimeSpan.FromSeconds(30), null);
                        })
                    .UseLazyLoadingProxies();
            });

            return services;
        }

        public static IServiceCollection AddCustomSignalR(this IServiceCollection services)
        {
            services
                .AddSignalR();

            return services;
        }

        public static IServiceCollection AddCustomOptions(this IServiceCollection services)
        {
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetails = new ValidationProblemDetails(context.ModelState)
                    {
                        Instance = context.HttpContext.Request.Path,
                        Status = StatusCodes.Status400BadRequest,
                        Detail = "Please refer to the errors property for additional details."
                    };

                    return new BadRequestObjectResult(problemDetails)
                    {
                        ContentTypes = { "application/problem+json", "application/problem+xml" }
                    };
                };
            });

            return services;
        }

        public static IServiceCollection AddCustomCAP(this IServiceCollection services, string version, string machine, string domain, IConfiguration configuration)
        {
            services
                .AddCap(x =>
                {
                    x.DefaultGroup = $"cap.queue.{version}.{machine}.{domain}";
                    x.UsePostgreSql(opt => { opt.ConnectionString = configuration["ConnectionString"]; });
                    x.UseRabbitMQ(options =>
                    {
                        options.HostName = configuration["RabbitMQ:HostName"];
                        options.UserName = configuration["RabbitMQ:UserName"];
                        options.Password = configuration["RabbitMQ:Password"];
                        options.VirtualHost = configuration["RabbitMQ:VirtualHost"];
                        options.Port = Convert.ToInt32(configuration["RabbitMQ:Port"]);
                        options.ExchangeName = $"cap.router.{version}.{machine}.{domain}";
                    });
                });

            return services;
        }

        public static IServiceCollection AddCustomIISOptions(this IServiceCollection services)
        {
            services.Configure<IISServerOptions>(options =>
            {
                options.AutomaticAuthentication = false;
            });

            return services;
        }
        
        public static IServiceCollection AddCustomLogging(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConfiguration(configuration.GetSection("Logging"));
                loggingBuilder.AddConsole();
                loggingBuilder.AddDebug();
                loggingBuilder.AddEventSourceLogger();
            });

            return services;
        }
        
        public static IServiceCollection AddCustomRedirect(this IServiceCollection services, int port)
        {
            services.AddHttpsRedirection(options =>
            {
                options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
                options.HttpsPort = port;
            });

            return services;
        }
        
        /// <summary>
        /// Viene specificata l'autenticazione e viene aggiunto JWT con i parametri specificati nelle appSettings
        /// </summary>
        public static (AuthenticationBuilder, IServiceCollection) AddCustomAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims

            var builder = services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                });

            return (builder, services);
        }
        
        public static IServiceCollection AddCustomJwt(this (AuthenticationBuilder, IServiceCollection) item, IConfiguration configuration)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims

            var (builder, services) = item;

            builder
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,  
                        ValidateIssuer = true,   
                        ValidateAudience = true,  
                        ValidateLifetime = true,
                        RequireExpirationTime = true,  
                        ValidIssuer = configuration["JwtIssuer"],
                        ValidAudience = configuration["JwtAudience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                });

            return services;
        }

        /// <summary>
        /// Autofac
        /// Vengono registrati per la DependencyInjection tutte le classi terminanti con "Service",
        /// EmailSender e la relativa interfaccia e il GenericRepository con la relativa interfaccia.
        /// </summary>
        public static IServiceProvider CustomStartup(this IServiceCollection services, Assembly dataAccess)
        {
            var container = new ContainerBuilder();
            
            container
                .RegisterAssemblyTypes(dataAccess)
                .Where(t => t.Name.EndsWith("Service"))
                .AsSelf()
                .InstancePerDependency();
            
            container
                .RegisterAssemblyTypes(typeof(EmailSender).Assembly)
                .AsImplementedInterfaces()
                .InstancePerDependency();

            container
                .RegisterGeneric(typeof(GenericRepository<,>))
                .AsImplementedInterfaces()
                .InstancePerDependency();
            
            container.Populate(services);
            
            return new AutofacServiceProvider(container.Build());
        }
        
        public static IServiceCollection AddCustomApiVersioning(this IServiceCollection services)
        {
            services
                .AddVersionedApiExplorer(opt =>
                {
                    // The format of the version added to the route URL
                    opt.GroupNameFormat = "'v'VVV";
                    // Tells swagger to replace the version in the controller route
                    opt.SubstituteApiVersionInUrl = true;
                })
                .AddApiVersioning(opt =>
                {
                    opt.ReportApiVersions = true;
                    opt.AssumeDefaultVersionWhenUnspecified = true;
                    opt.DefaultApiVersion = new ApiVersion(1, 0);
                });

            return services;
        }
        
        public static IServiceCollection AddCustomSwagger(this IServiceCollection services,
            string title, string baseDirectory, string xmlFile)
        {
            services.AddSwaggerGen(options =>
            {
                // Resolve the temprary IApiVersionDescriptionProvider service
                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                
                options.DescribeAllEnumsAsStrings();
                
                // Add a swagger document for each discovered API version
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    options.SwaggerDoc(description.GroupName, new Info
                    {
                        Title = $"{title} {description.ApiVersion}",
                        Version = description.ApiVersion.ToString(),
                        Description = description.IsDeprecated ? $"{title} - DEPRECATED" : title,
                        TermsOfService = "Terms Of Service",
                        Contact = new Contact
                        {
                            Name = "Nicola Taddei",
                            Email = "developer@componentsengine.it",
                            Url = string.Empty
                        }
                    });
                }
                
                // Add a custom filter for settint the default values  
                options.OperationFilter<SwaggerDefaultValues>();

                // Set the comments path for the Swagger JSON and UI.
                var xmlPath = Path.Combine(baseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);

                // Enable automatic annotation for examples pt.1
                options.ExampleFilters();
                
                options.CustomSchemaIds(SchemaStrategy);
            });

            // Enable automatic annotation for examples pt.2
            services.AddSwaggerExamples();

            return services;
        }
        
        /// <summary>
        /// Schema strategy di swagger per levare i suffissi delle classi quando genera la documentazione
        /// </summary>
        private static string SchemaStrategy(Type currentClass)
        {
            var returnedValue = currentClass.Name;

            returnedValue = returnedValue.Contains("Dto") ? returnedValue.Replace("Dto", string.Empty) : returnedValue;
            returnedValue = returnedValue.Contains("`1") ? returnedValue.Replace("`1", string.Empty) : returnedValue;
            return returnedValue;
        }
    }
}