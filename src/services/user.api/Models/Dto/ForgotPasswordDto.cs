namespace user.api.Models.Dto
{
    public class ForgotPasswordRequestDto
    {
        public string Username { get; set; }
        public string Url { get; set; }
    }
}