using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace user.api.Models.Entities
{
    public class Permission
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
        public virtual List<RolePermission> RolePermissions { get; set; }
    }
    
    public class PermissionConfiguration : IEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> builder)
        {
            builder
                .ToTable("Permission");
            
            builder
                .HasKey(k => k.Id);
            
            builder
                .HasIndex(c => c.Name)
                .IsUnique();
            
            builder
                .Property(c => c.Id)
                .ValueGeneratedOnAdd();
            
            builder
                .HasMany(c => c.RolePermissions)
                .WithOne(c => c.Permission)
                .HasForeignKey(c => c.PermissionId);
        }
    }
}