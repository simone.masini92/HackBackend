using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Castle.Core.Internal;
using DotNetCore.CAP;
using infrastructure.Repositories;
using models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using user.api.Infrastructure;
using user.api.Models.Dto;
using user.api.Models.Entities;
using utilities.Class;
using utilities.Extension;
#pragma warning disable 4014

namespace user.api.Services
{
    public class UserService
    {
        private readonly IMapper _mapper;
        private readonly ILogger<UserService> _logger;
        private readonly UserManager<User> _userManager;
        private readonly IGenericRepository<User, UserContext> _userGenericRepository;

        public UserService(
            IMapper mapper,
            ILogger<UserService> logger,
            UserManager<User> userManager,
            IGenericRepository<User, UserContext> userGenericRepository)
        {
            _mapper = mapper;
            _logger = logger;
            _userManager = userManager;
            _userGenericRepository = userGenericRepository;
        }

        public async Task<UserResponseDto> GetUser(Guid userId)
        {
            var user = await _userGenericRepository.GetByKeyAsync(userId);
            return _mapper.Map<UserResponseDto>(user);
        }

        public async Task<bool> HasHash(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            return await _userManager.HasPasswordAsync(user);
        }
    }
}