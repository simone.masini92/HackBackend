using System.Collections.Generic;

namespace setup.api.Models.Dto
{
    public class ExportResponseDto
    {
        public string TableName { get; set; }
        public List<string> Columns { get; set; }
        public string ExportUrl { get; set; }
    }
}