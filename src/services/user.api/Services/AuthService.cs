using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Castle.Core.Internal;
using infrastructure.Hubs;
using infrastructure.Repositories;
using infrastructure.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using user.api.Infrastructure;
using user.api.Models.Dto;
using user.api.Models.Entities;
using utilities.Extension;
using User = user.api.Models.Entities.User;

namespace user.api.Services
{
    public class AuthService
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<AuthService> _logger;
        private readonly UserSettings _options;
        private readonly IEmailSender _emailSender;
        private readonly IHubContext<SignalRAuthHub> _hub;
        private readonly IGenericRepository<RolePermission, UserContext> _rolePermissionRepo;
        
        public AuthService(
            SignInManager<User> signInManager,
            UserManager<User> userManager,
            IOptions<UserSettings> options,
            ILogger<AuthService> logger,
            IEmailSender emailSender,
            IHubContext<SignalRAuthHub> hub,
            IGenericRepository<RolePermission, UserContext> rolePermissionRepo)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _options = options.Value;
            _logger = logger;
            _emailSender = emailSender;
            _hub = hub;
            _rolePermissionRepo = rolePermissionRepo;
        }
        
        public async Task<LoginResponseDto> SignIn(LoginRequestDto model)
        {
            var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, true);

            if (result.IsLockedOut)
            {
                var user = await _userManager.FindByNameAsync(model.Username);
                throw new Exception($"Locked Out until {user.LockoutEnd}");
            }
            
            if (!result.Succeeded)
            {
                bool isEmailConfirmed;
                try
                {
                    isEmailConfirmed = await _userManager.IsEmailConfirmedAsync(await _userManager.FindByNameAsync(model.Username));
                }
                catch (Exception)
                {
                    throw new Exception("Login failed. Please check username and password");
                }
                
                if (!isEmailConfirmed)
                {
                    throw new Exception("Email not confirmed");
                }

                throw new Exception("Login Failed");
            }
            
            var appUser = _userManager.Users.SingleOrDefault(r => r.UserName == model.Username);

            if (appUser == null || appUser.Deleted)
            {
                throw new Exception("User not exist");
            }

            var validateActiveDate = (appUser.ActiveFrom <= DateTime.Now || !appUser.ActiveFrom.HasValue) 
                                     &&
                                     (appUser.ActiveTo > DateTime.Now || !appUser.ActiveTo.HasValue);

            if (!validateActiveDate || !appUser.Active)
            {
                throw new Exception("User not active");
            }

            var changePassword = appUser.PasswordExpirationDate <= DateTime.Now || appUser.ForceResetPassword;

            if (changePassword)
            {
                return new LoginResponseDto(Guid.Empty, null, null, null, null, false, true);
            }
            var temporary = appUser.LoginExpiration == 1;

            _logger.LogInformation("Generating token...");
            var token = await GenerateJwtToken(appUser);
            await UpdateLastLogin(appUser.UserName);
            return new LoginResponseDto(appUser.Id, appUser.Email, appUser.UserName, token, appUser.Image, temporary, false);
        }

        public async Task SignOut()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task SignUp(RegisterRequestDto model)
        {
            var user = new User
            {
                UserName = model.Username,
                Email = model.Email,
                Active = true
            };

            _logger.LogInformation("Creating user...");
            var result = await _userManager.CreateAsync(user, model.Password);
            result.ThrowIdentityError(_logger);
            
            user.PasswordHistories = new List<PasswordHistory>(new []{
                new PasswordHistory
                {
                    CreateDate = DateTime.Now,
                    PasswordHash = user.PasswordHash
                }
            });

            // TODO: perché qui non l'ho gestita con l'identity result?
            try
            {
                await _userManager.UpdateAsync(user);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            
            
            _logger.LogInformation("Created!");
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callback = model.Url + "?userId=" + user.Id + "&code=" + code;
            await _emailSender.SendEmailAsync(model.Email, "Confirm your email",
                $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callback)}'>clicking here</a>.");
        }

        public async Task ConfirmEmail(Guid userId, string code)
        {
            var adjustingCode = code.Replace(" ", "+");
            var user = await _userManager.FindByIdAsync(userId.ToString());
            var result = await _userManager.ConfirmEmailAsync(user, adjustingCode);
            result.ThrowIdentityError(_logger);
            await _userManager.UpdateAsync(user);
        }
        
        public async Task ForgotPassword(ForgotPasswordRequestDto model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user == null || !await _userManager.IsEmailConfirmedAsync(user))
            {
                throw new Exception("User doesn't exist or email not confirmed");
            }
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            var callback = model.Url + "?userId=" + user.Id + "&code=" + code;
            await _emailSender.SendEmailAsync(user.Email, "Reset Password",
                $"Please reset your password by <a href='{HtmlEncoder.Default.Encode(callback)}'>clicking here</a>.");
        }
        
        public async Task ResetPassword(ResetPasswordRequestDto model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            var adjustingCode = model.Token.Replace(" ", "+");
            var result = await _userManager.ResetPasswordAsync(user, adjustingCode, model.Password);
            result.ThrowIdentityError(_logger);
        }
        
        public async Task ChangePassword(ChangePasswordRequestDto passwords)
        {
            var user = await _userManager.FindByNameAsync(passwords.Username);
            var limit = user.PasswordHistoryLimit;
            var lastPasswords = user.PasswordHistories.OrderByDescending(c => c.CreateDate).Take(limit).ToList();
            lastPasswords.ForEach(password =>
            {
                var verifyHash = _userManager.PasswordHasher.VerifyHashedPassword(user, password.PasswordHash, passwords.NewPassword);
                if (verifyHash == PasswordVerificationResult.Success)
                {
                    throw new Exception("Password find in history");
                }
            });
            if (passwords.OldPassword.IsNullOrEmpty() && user.PasswordHash.IsNullOrEmpty())
            {
                var result = await _userManager.AddPasswordAsync(user, passwords.NewPassword);
                result.ThrowIdentityError(_logger);
            }
            else
            {
                var res = await _userManager.ChangePasswordAsync(user, passwords.OldPassword, passwords.NewPassword);
                res.ThrowIdentityError(_logger);
            }
            
            user.PasswordHistories.Add(new PasswordHistory
            {
                CreateDate = DateTime.Now,
                PasswordHash = user.PasswordHash
            });
            user.ForceResetPassword = false;
            user.PasswordExpirationDate = DateTime.Now.AddMonths(3);
            await _userManager.UpdateAsync(user);
        }
        
        private async Task<string> GenerateJwtToken(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email ?? "info@some.com"),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.NameId, user.Id.ToString())
            };

            var permissions = await _rolePermissionRepo
                .GetAllWhere(c => c.RoleId == user.RoleId)
                .Select(h => h.Permission)
                .ToListAsync();
            
            if (permissions.Any())
            {
                permissions.ForEach(permission =>
                {
                    claims.Add(new Claim(ClaimTypes.Role, $"{permission.Name}"));
                });
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.JwtKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                _options.JwtIssuer,
                _options.JwtAudience,
                claims,
                expires: SwitchLoginExpired(user.LoginExpiration),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private static DateTime SwitchLoginExpired(int option)
        {
            switch (option)
            {
                case 1: return DateTime.Now.AddHours(10);
                case 2: return DateTime.Now.AddHours(1);
                case 3: return DateTime.Now.AddHours(6);
                case 4: return DateTime.Now.AddHours(12);
                case 5: return DateTime.Now.AddDays(1);
                case 6: return DateTime.Now.AddDays(2);
                case 7: return DateTime.Now.AddDays(7);
                case 8: return DateTime.Now.AddMonths(1);
                case 9: return DateTime.Now.AddMonths(2);
                case 10: return DateTime.Now.AddMonths(6);
                case 11: return DateTime.Now.AddYears(1);
                case 12: return DateTime.Now.AddYears(2);
                case 13: return DateTime.Now.AddYears(10);
                default: return DateTime.Now.AddHours(10);
            }
        }

        private async Task UpdateLastLogin(string username)
        {
            var user = await _userManager.FindByNameAsync(username);
            user.LastLogin = DateTime.Now;
            await _userManager.UpdateAsync(user);
        }

        public static void SetConnectionId(string connectionId)
        {
            Singleton.Instance.WebSocketConnectionId = connectionId;
        }
    }
    
    public class Singleton
    {
        private static readonly Lazy<Singleton> Lazy = new Lazy<Singleton>(() => new Singleton());
        public string WebSocketConnectionId;
        private Singleton()
        {
        } 
        public static Singleton Instance => Lazy.Value; 
    }
}