﻿using AutoMapper;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Castle.Core.Logging;
using catalog.api.Infrastructure;
using catalog.api.Models.Dto.Request;
using catalog.api.Models.Dto.Response;
using catalog.api.Models.Entities;
using infrastructure.Repositories;
using Microsoft.Extensions.Logging;
using models;
using utilities.Class;

namespace catalog.api.Services
{
    public class CatalogItemService
    {
        private readonly IGenericRepository<CatalogItem, CatalogContext> _itemGenericRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<CatalogItemService> _logger;

        public CatalogItemService(
            IGenericRepository<CatalogItem, CatalogContext> itemGenericRepository, 
            IMapper mapper, 
            ILogger<CatalogItemService> logger)
        {
            _itemGenericRepository = itemGenericRepository;
            _mapper = mapper;
            _logger = logger;
        }

    }
}