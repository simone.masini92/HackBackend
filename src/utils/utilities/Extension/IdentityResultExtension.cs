using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace utilities.Extension
{
    public static class IdentityResultExtension
    {
        public static void ThrowIdentityError(this IdentityResult identityResult, ILogger logger = null)
        {
            if (identityResult.Succeeded) return;
            foreach (var identityError in identityResult.Errors)
            {
                logger?.LogError(identityError.Description);
                throw new Exception(identityError.Description);
            }
        }

        public static void ThrowIdentityError(this IdentityResult[] results, ILogger logger)
        {
            foreach (var result in results)
            {
                result.ThrowIdentityError(logger);
            }
        }
    }
}