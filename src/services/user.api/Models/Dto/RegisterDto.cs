namespace user.api.Models.Dto
{
    public class RegisterRequestDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Fullname { get; set; }
        public string Username { get; set; }
        public string Url { get; set; }
    }
}