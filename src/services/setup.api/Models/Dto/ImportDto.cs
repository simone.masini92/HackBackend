using System;

namespace setup.api.Models.Dto
{
    public class ImportResponseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Pivots { get; set; }
    }
}