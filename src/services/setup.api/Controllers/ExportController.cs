using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using setup.api.Services;

namespace setup.api.Controllers
{
    [ApiVersion("1.0")]
    [Route("/api/v{version:apiVersion}/setups/exports")]
    [ApiController]
    [Authorize]
    public class ExportController : ControllerBase
    {
        private readonly ExportService _exportService;
        private readonly ILogger<ExportController> _logger;

        public ExportController(
            ExportService exportService,
            ILogger<ExportController> logger)
        {
            _exportService = exportService;
            _logger = logger;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAllTables()
        {
            var tables = await _exportService.GetAllTables();
            return Ok(tables);
        }
    }
}