﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;
using setup.api.Infrastructure.Seed;
using setup.api.Models.Entities;

namespace setup.api.Infrastructure
{
    public class SetupContextSeed
    {
        public async Task SeedAsync(SetupContext context, IHostingEnvironment env, ILogger<SetupContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(SetupContextSeed));

            await policy.ExecuteAsync(async () =>
            {
                var contentRootPath = env.ContentRootPath;

                if (!context.DataTypesDictionary.Any())
                {
                    await context.DataTypesDictionary.AddRangeAsync(DataTypeSeed.GetFromFile(contentRootPath, logger));
                    await context.SaveChangesAsync();
                }
                
                if (!context.InputTypesDictionary.Any())
                {
                    await context.InputTypesDictionary.AddRangeAsync(InputTypeSeed.GetFromFile(contentRootPath, logger));
                    await context.SaveChangesAsync();
                }
                
                if (!context.OperationTypesDictionary.Any())
                {
                    await context.OperationTypesDictionary.AddRangeAsync(OperationTypeSeed.GetFromFile(contentRootPath, logger));
                    await context.SaveChangesAsync();
                }

                if (!context.TablesSetup.Any())
                {
                    List<Table> setups;
                    using (var r = new StreamReader($"{contentRootPath}/Setup/SetupsSeed.json"))
                    {
                        var json = r.ReadToEnd();
                        setups = JsonConvert.DeserializeObject<List<Table>>(json);
                    }

                    await context.TablesSetup.AddRangeAsync(setups);
                    await context.SaveChangesAsync();
                }
            });
        }

        private static AsyncRetryPolicy CreatePolicy(ILogger logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>()
                .WaitAndRetryAsync(
                    retries,
                    retry => TimeSpan.FromSeconds(5),
                    (exception, timeSpan, retry, ctx) =>
                    {
                        logger.LogTrace($"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
                    }
                );
        }
    }
}
