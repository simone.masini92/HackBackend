<?xml version="1.0" encoding="utf-8" ?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      autoReload="true"
      internalLogLevel="Info"
      internalLogFile=".logs\internal-nlog.txt">

    <!-- enable asp.net core layout renderers -->
    <extensions>
        <add assembly="NLog.Web.AspNetCore"/>
    </extensions>

    <!-- the targets to write to -->
    <targets>
        <!-- write logs to file  -->
        <target xsi:type="File" name="allfile" fileName=".logs\log-all-${shortdate}.log"
                layout="${longdate}|${event-properties:item=EventId_Id}|${uppercase:${level}}|${logger}|${message} ${exception:format=tostring}" />

        <!-- another file log, only own logs. Uses some ASP.NET core renderers -->
        <target xsi:type="File" name="ownFileDetailed-web" fileName=".logs\log-own-detailed-${shortdate}.log"
                layout="${longdate}|${event-properties:item=EventId_Id}|${uppercase:${level}}||${logger}|${message} ${exception:format=tostring}|url: ${aspnet-request-url}|action: ${aspnet-mvc-action}" />

        <!-- another file log, only own logs. Uses some ASP.NET core renderers -->
        <target xsi:type="File" name="ownFile-web" fileName=".logs\log-own-short-${shortdate}.log"
                layout="${longdate}||${uppercase:${level}}||${message} ${exception:format=tostring}" />

        <target xsi:type="File" name="queryInterceptor" fileName=".logs\queryInterceptor-${shortdate}.log"
                layout="${longdate}${message} ${exception:format=tostring}" />

    </targets>

    <!-- rules to map from logger name to target -->
    <rules>
        <!--All logs, including from Microsoft-->
        <logger name="*" minlevel="Trace" writeTo="allfile" />

        <!--Skip non-critical Microsoft logs and so log only own logs-->
        <logger name="Microsoft.*" final="true" />
        <!-- BlackHole without writeTo -->
        <logger name="*" minlevel="Trace" writeTo="ownFile-web" />
        <logger name="*" minlevel="Trace" writeTo="ownFileDetailed-web" />

        <logger name="*.QueryInterceptor" minlevel="Info" writeTo="queryInterceptor" />
    </rules>
</nlog>