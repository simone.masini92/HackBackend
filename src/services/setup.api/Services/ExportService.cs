using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Internal;
using infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using setup.api.Infrastructure;
using setup.api.Models.Dto;
using setup.api.Models.Entities;
using utilities.Class;

namespace setup.api.Services
{
    public class ExportService
    {
        private readonly ILogger<ExportService> _logger;
        private readonly IGenericRepository<Table, SetupContext> _tableRepository;
        private readonly IGenericRepository<DataManagement, SetupContext> _exportRepository;

        public ExportService(
            ILogger<ExportService> logger,
            IGenericRepository<Table, SetupContext> tableRepository,
            IGenericRepository<DataManagement, SetupContext> exportRepository)
        {
            _logger = logger;
            _tableRepository = tableRepository;
            _exportRepository = exportRepository;
        }
        
        public async Task<List<ExportResponseDto>> GetAllTables()
        {
            var tables = await _exportRepository
                .GetAll()
                .OrderBy(c => c.Table)
                .ToListAsync();
            
            var exportInfo = new List<ExportResponseDto>();
            
            foreach (var table in tables)
            {
                var columnsToExport = new List<string>();
                var tablesName = table.Table.Split(',').Trim();
                
                foreach (var tableName in tablesName)
                {
                    var tablem = await _tableRepository.FirstOrDefaultAsync(c => c.Name == tableName);
                    var columns = tablem.Columns.Where(c =>
                        c.ColumnOperations.Any(a => (a.OperationType == "VIEW" || a.OperationType == "ALL") && a.Hidden == false && a.Avoid == false));
                    columnsToExport.AddRange(columns.Select(c => c.Name));
                }
                
                exportInfo.Add(new ExportResponseDto
                {
                    Columns = columnsToExport,
                    ExportUrl = table.ExportUrl,
                    TableName = table.Table
                });
            }

            return exportInfo.Where(c => !c.ExportUrl.IsNullOrEmpty()).ToList();
        }
    }
}