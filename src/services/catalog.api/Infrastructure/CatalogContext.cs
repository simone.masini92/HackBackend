﻿using catalog.api.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace catalog.api.Infrastructure
{
    public class CatalogContext : DbContext
    {
        public CatalogContext(DbContextOptions options) : base(options) { }
        
        //============= DbSet delle tabelle ===========//
        public DbSet<CatalogItem> CatalogItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.ApplyConfiguration(new CatalogItemConfiguration());
        }
    }
}
