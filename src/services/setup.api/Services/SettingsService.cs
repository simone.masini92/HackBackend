using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using setup.api.Infrastructure;
using setup.api.Models.Dto;
using setup.api.Models.Entities;

namespace setup.api.Services
{
    public class SettingsService
    {
        private readonly ILogger<Settings> _logger;
        private readonly IGenericRepository<Settings, SetupContext> _configurationRespository;
        private readonly IMapper _mapper;

        public SettingsService(ILogger<Settings> logger,
            IGenericRepository<Settings, SetupContext> configurationRespository,
            IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _configurationRespository = configurationRespository;
        }

        public async Task<List<SettingsDto>> GetAllSettings()
        {
            var configs = await _configurationRespository.GetAll().ToListAsync();
            return _mapper.Map<List<SettingsDto>>(configs);
        }
    }
}