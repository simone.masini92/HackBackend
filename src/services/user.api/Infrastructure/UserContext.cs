using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using user.api.Models.Entities;

namespace user.api.Infrastructure
{
    public class UserContext : IdentityUserContext<User, Guid, UserClaim, UserLogin, UserToken>
    {
        public UserContext(DbContextOptions options) : base(options) { }
        
        //============= DbSet delle tabelle ===========//
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<Permission> Permissions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            /* IDENTITY */
            
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new UserClaimConfiguration());
            modelBuilder.ApplyConfiguration(new UserLoginConfiguration());
            modelBuilder.ApplyConfiguration(new UserTokenConfiguration());
            
            /* ROLE AND PERMISSION */
            
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new RolePermissionConfiguration());
            modelBuilder.ApplyConfiguration(new PermissionConfiguration());
            
            /* PERSONAL */

            modelBuilder.ApplyConfiguration(new PasswordHistoryConfiguration());
        }
    }
}