using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using models;

namespace setup.api.Models.Entities
{
    public class DataManagement
    {
        public Guid Id { get; set; }
        public string Table { get; set; }
        public string ImportUrl { get; set; }
        public string ExportUrl { get; set; }
        public string Operations { get; set; }
        public string Pivots { get; set; }
        public string SyncUrl { get; set; }
        public bool ToSync { get; set; }
        public bool Syncing { get; set; }
    }
    
    public class ImportConfiguration : IEntityTypeConfiguration<DataManagement>
    {
        public void Configure(EntityTypeBuilder<DataManagement> builder)
        {
            builder
                .ToTable("DataManagement");

            builder
                .Property(c => c.Table)
                .HasColumnName("Table(s)");

            builder
                .HasKey(c => c.Id);
            
            builder
                .Property(c => c.Id)
                .HasDefaultValueSql("NEWID()");

            builder
                .HasIndex(c => c.Table)
                .IsUnique();

            builder
                .Property(c => c.Operations)
                .IsRequired()
                .HasDefaultValue($"{ImportOperation.Add.ToString()}, {ImportOperation.Both.ToString()}, {ImportOperation.Update.ToString()}");

            builder
                .Property(c => c.ToSync)
                .HasDefaultValue(false);
            
            builder
                .Property(c => c.Syncing)
                .HasDefaultValue(false);
        }
    }
}