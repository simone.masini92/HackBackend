using System.Collections.Generic;

namespace setup.api.Models.Dto
{
    public class SetupMiddleDto
    {
        public List<ViewColumnResponseDto> ViewColumns { get; set; }
        public List<AddEditColumnResponseDto> AddColumns { get; set; }
        public List<AddEditColumnResponseDto> EditColumns { get; set; }
    }

    public class SetupResponseDto
    {
        public List<string> PropertiesToSelect { get; set; }
        public List<ViewColumnResponseDto> ViewColumns { get; set; }
        public dynamic AddColumns { get; set; }
        public dynamic EditColumns { get; set; }
        public string Key { get; set; }
    }

    public class SingleColumnDto
    {
        public string Name { get; set; }
        public bool Hidden { get; set; }
        public bool Editable { get; set; }
        public string DataType { get; set; }
        public string InputType { get; set; }
        public string InputResolverList { get; set; }
        public string InputResolverUrl { get; set; }
        public bool Nullable { get; set; }
    }

    public class SingleSetupResponseDto
    {
        public List<string> PropertiesToSelect { get; set; }
        public string Key { get; set; }
        public List<SingleColumnDto> Properties { get; set; }
    }
    
    
}