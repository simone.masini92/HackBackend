using System.Threading.Tasks;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using models;
using setup.api.Services;

namespace setup.api.Controllers
{
    [ApiVersion("1.0")]
    [Route("/api/v{version:apiVersion}/setups/syncs")]
    [ApiController]
    [Authorize]
    public class SyncController : ControllerBase
    {
        private readonly SyncService _syncService;
        private readonly ILogger<SyncController> _logger;

        public SyncController(
            SyncService syncService,
            ILogger<SyncController> logger)
        {
            _syncService = syncService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> SetSynchroFromHttp([FromBody] SyncMessageDto message)
        {
            await _syncService.SetSynchro(message);
            return Ok();
        }
        
        [HttpGet]
        public async Task<IActionResult> GetSyncTables()
        {
            var result = await _syncService.GetSyncTables();
            return Ok(result);
        }
        
        [ApiExplorerSettings(IgnoreApi = true)]
        [CapSubscribe("set.to.sync")]
        public async Task SetSynchroFromAsyncMessage(SyncMessageDto message)
        {
            await _syncService.SetSynchro(message);
        }
        
        [ApiExplorerSettings(IgnoreApi = true)]
        [CapSubscribe("set.syncing")]
        public async Task SetSyncing(SyncMessageDto message)
        {
            await _syncService.SetSyncing(message);
        }
        
        [ApiExplorerSettings(IgnoreApi = true)]
        [CapSubscribe("sync.percentage")]
        public void SyncPercentage(SignalRSyncPercentage message)
        {
            _syncService.SyncPercentage(message);
        }
    }
}