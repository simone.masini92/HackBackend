using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace utilities.Class
{
    public class JwtUtility
    {
        /// <summary>
        /// Prendo il token, lo lavoro e lo restituisco in un formato leggibile
        /// </summary>
        /// <param name="tokenToRead">Il token da elaborare</param>
        /// <returns>Restituisco il token leggibile</returns>
        /// <exception cref="Exception">Se il token non è leggibile produco eccezione</exception>
        private static JwtSecurityToken GetReadedToken(string tokenToRead)
        {
            tokenToRead = tokenToRead.Replace("Bearer ", "").Trim();
            var jwtHandler = new JwtSecurityTokenHandler();
            var readableToken = jwtHandler.CanReadToken(tokenToRead);
            if (!readableToken)
            {
                throw new Exception("The token doesn't seem to be in a proper JWT format");
            }

            return jwtHandler.ReadJwtToken(tokenToRead); 
        }

        /// <summary>
        /// Leggo i permessi contenuti nel token
        /// </summary>
        /// <param name="tokenToRead">Il token da eleborare</param>
        /// <returns>Ritorno la lista dei permessi</returns>
        public static List<string> GetPermissionsFromToken(string tokenToRead)
        {
            var token = GetReadedToken(tokenToRead);
            var roleClaims = token.Claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return roleClaims.Select(c => c.Value).ToList();
        }

        /// <summary>
        /// Leggo nel token la claim NameId che è quella che contiene lo userId
        /// </summary>
        /// <param name="request">La chiamata Http ricevuta</param>
        /// <returns>Ritorna il GUID dello User id</returns>
        public static Guid GetUserId(HttpRequest request)
        {
            var tokenToRead = request.Headers["Authorization"];
            var token = GetReadedToken(tokenToRead);
            var userIdString = token.Claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.NameId)?.Value;
            return Guid.Parse(userIdString ?? throw new Exception("User id not found"));
        }
    }
}