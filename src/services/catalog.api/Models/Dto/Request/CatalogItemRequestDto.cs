namespace catalog.api.Models.Dto.Request
{
    public class CatalogItemRequestDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? Discount { get; set; }
    }
}