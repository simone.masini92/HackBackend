using System;
using System.Collections.Generic;

namespace setup.api.Models.Dto
{
    public class ColumnDto
    {
        public string Name { get; set; }
        public bool Nullable { get; set; }
        public int? Sort { get; set; }
        public string DataType { get; set; }
        public string InputType { get; set; }
        public string InputResolverUrl { get; set; }
        public string InputResolverList { get; set; }
        public string Group1 { get; set; }
        public string Group2 { get; set; }
        public List<ColumnOperationResponseDto> ColumnOperations { get; set; }
    }

    public class ColumnResponseDto : ColumnDto
    {
        public Guid Id { get; set; }
    }

    public class ColumnRequestDto : ColumnDto
    {
        public Guid? Id { get; set; }
    }

    public class ColumnAddRequestDto
    {
        public Guid TableId { get; set; }
        public string ColumnName { get; set; }
    }
    
    public class ViewColumnResponseDto
    {
        public string Name { get; set; }
        public bool? Hidden { get; set; }
    }

    public class AddEditColumnResponseDto
    {
        public string Name { get; set; }
        public string DataType { get; set; }
        public string InputType { get; set; }
        public string InputResolverUrl { get; set; }
        public string InputResolverList { get; set; }
        public bool Hidden { get; set; }
        public bool Nullable { get; set; }
        public string Group1 { get; set; }
        public string Group2 { get; set; }
    }
}