using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Castle.Core;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using models;
using setup.api.Services;
using utilities.Class;

namespace setup.api.Controllers
{
    [ApiVersion("1.0")]
    [Route("/api/v{version:apiVersion}/setups/imports")]
    [ApiController]
    [Authorize]
    public class ImportController : ControllerBase
    {
        private readonly IHostingEnvironment _env;
        private readonly ImportService _importService;
        private readonly ILogger<ImportController> _logger;

        public ImportController(
            ImportService importService,
            ILogger<ImportController> logger,
            IHostingEnvironment env)
        {
            _importService = importService;
            _logger = logger;
            _env = env;
        }
        
        [HttpGet("tables")]
        public async Task<IActionResult> GetAllTables()
        {
            var tables = await _importService.GetAllTables();
            return Ok(tables);
        }

        [HttpGet("operations")]
        public IActionResult GetImportOperations()
        {
            var operationsValues = Enum.GetValues(typeof(ImportOperation));
            var operationsNames = Enum.GetNames(typeof(ImportOperation));
            return Ok(new Pair<string[], Array>(operationsNames, operationsValues));
        }
        
        [HttpGet("urls")]
        public async Task<IActionResult> GetAllUrls()
        {
            var urls = await _importService.GetAllUrls();
            return Ok(urls);
        }

        [HttpGet]
        public IActionResult ListFiles()
        {
            var files = _importService.ListFiles(_env.ContentRootPath);
            return Ok(files);
        }

        [HttpGet("example-list/{filePath}")]
        public IActionResult ExampleList(string filePath)
        {
            var exampleList = ImportFile.GetExampleList(filePath, 5);
            return Ok(exampleList);
        }
        
        [HttpGet("table-import-properties/{tableId}")]
        public async Task<IActionResult> TableImportProperties(Guid tableId)
        {
            var properties = await _importService.TableImportProperties(tableId);
            return Ok(properties);
        }
        
        [HttpPost("upload")]
        public IActionResult UploadFile()
        {
            using (var sr = new StreamReader(Request.Body))
            {
                var body = sr.BaseStream;
                var parser = new MultipartParser(body);
                var fileContent = Encoding.Default.GetString(parser.FileContents);
                ImportFile.UploadFile(fileContent, $"{_env.ContentRootPath}/SavedFiles", parser.Filename);
            }
            return Ok();
        }

        [HttpDelete("delete-file/{filePath}")]
        public IActionResult DeleteFile(string filePath)
        {
            System.IO.File.Delete(filePath);
            return Ok();
        }
        
        [ApiExplorerSettings(IgnoreApi = true)]
        [CapSubscribe("import.percentage")]
        public void ImportPercentage(SignalRImporting message)
        {
            _importService.ImportPercentage(message);
        }
        
        [ApiExplorerSettings(IgnoreApi = true)]
        [CapSubscribe("import.result")]
        public void ImportResult(SignalRImportResult result)
        {
            _importService.ImportResult(result);
        }
    }
}