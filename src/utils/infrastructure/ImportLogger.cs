using Microsoft.Extensions.Logging;

namespace infrastructure
{
    public class ImportLogger
    {
        public static void Log(ILogger logger, string error)
        {
            logger.LogError(error);
        }
    }
}