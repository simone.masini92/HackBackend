namespace setup.api.Models.Dto
{
    public class ColumnPermissionResponseDto
    {
        public string Permission { get; set; }
        public bool? Hidden { get; set; }
    }
}