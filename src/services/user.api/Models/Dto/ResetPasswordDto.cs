namespace user.api.Models.Dto
{
    public class ResetPasswordRequestDto
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string Password { get; set; }
    }
}