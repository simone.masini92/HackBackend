namespace utilities.Class
{
    public static class ArrayStringUtility
    {
        public static string[] Trim(this string[] arrayString)
        {
            var newArray = new string[arrayString.Length];
            for (var i = 0; i < arrayString.Length; i++)
            {
                newArray[i] = arrayString[i].Trim();
            }

            return newArray;
        }
    }
}