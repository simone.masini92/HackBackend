using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace setup.api.Models.Entities
{
    public class Column
    {
        public Guid Id { get; set; }
        public Guid TableId { get; set; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
        public bool Nullable { get; set; }
        public int? Sort { get; set; }
        public string DataType { get; set; }
        public string InputType { get; set; }
        public string InputResolverUrl { get; set; }
        public string InputResolverList { get; set; }
        public string Group1 { get; set; }
        public string Group2 { get; set; }
        
        public virtual List<ColumnOperation> ColumnOperations { get; set; }
        public virtual Table Table { get; set; }
    }
    
    public class ColumnConfiguration : IEntityTypeConfiguration<Column>
    {
        public void Configure(EntityTypeBuilder<Column> builder)
        {
            builder
                .ToTable("Column");

            builder
                .HasKey(c => c.Id);
            
            builder
                .Property(c => c.Id)
                .HasDefaultValueSql("NEWID()");

            builder
                .HasIndex(t => new {t.Name, t.TableId})
                .IsUnique();

            builder
                .HasMany(c => c.ColumnOperations)
                .WithOne(c => c.Column)
                .HasForeignKey(c => c.ColumnId);
        }
    }
}