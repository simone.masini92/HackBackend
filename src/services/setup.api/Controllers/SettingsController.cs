using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using setup.api.Services;

namespace setup.api.Controllers
{
    [ApiVersion("1.0")]
    [Route("/api/v{version:apiVersion}/setups/settings")]
    [ApiController]
    [Authorize]
    public class SettingsController : ControllerBase
    {
        private readonly SettingsService _settingsService;
        private readonly ILogger<SettingsController> _logger;

        public SettingsController(
            SettingsService settingsService,
            ILogger<SettingsController> logger)
        {
            _settingsService = settingsService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllSettings()
        {
            var result = await _settingsService.GetAllSettings();
            return Ok(result);
        }
    }
}