﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using catalog.api.Infrastructure.Seed;
using Polly.Retry;

namespace catalog.api.Infrastructure
{
    public class CatalogContextSeed
    {
        public async Task SeedAsync(CatalogContext context, IHostingEnvironment env, ILogger<CatalogContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(CatalogContextSeed));

#pragma warning disable 1998
            await policy.ExecuteAsync(async () =>
#pragma warning restore 1998
            {
                var contentRootPath = env.ContentRootPath;

                if (!context.CatalogItems.Any())
                {
                    /*await context.CatalogItems.AddRangeAsync(CatalogItemSeed.GetFromFile(contentRootPath, logger));
                    await context.SaveChangesAsync();*/
                }
            });
        }

        private static AsyncRetryPolicy CreatePolicy(ILogger logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>()
                .WaitAndRetryAsync(
                    retries,
                    retry => TimeSpan.FromSeconds(5),
                    (exception, timeSpan, retry, ctx) =>
                    {
                        logger.LogTrace($"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
                    }
                );
        }
    }
}
