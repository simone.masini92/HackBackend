using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace setup.api.Models.Entities
{
    public class DataTypeDictionary
    {
        public string DataType { get; set; }
    }
    
    public class DataTypeDictionaryConfiguration : IEntityTypeConfiguration<DataTypeDictionary>
    {
        public void Configure(EntityTypeBuilder<DataTypeDictionary> builder)
        {
            builder
                .ToTable("DataTypeDictionary");

            builder
                .HasKey(c => c.DataType);
        }
    }
}