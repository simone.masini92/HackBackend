using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace setup.api.Models.Entities
{
    public class Settings
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    
    public class SettingsConfiguration : IEntityTypeConfiguration<Settings>
    {
        public void Configure(EntityTypeBuilder<Settings> builder)
        {
            builder
                .ToTable("Settings");

            builder
                .HasKey(c => c.Key);
        }
    }
}