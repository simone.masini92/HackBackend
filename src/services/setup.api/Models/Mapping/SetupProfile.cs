using System;
using System.IO;
using System.Linq;
using AutoMapper;
using models;
using setup.api.Models.Dto;
using setup.api.Models.Entities;

namespace setup.api.Models.Mapping
{
    public class SetupProfile : Profile
    {
        public SetupProfile()
        {
            CreateMap<Table, SetupMiddleDto>()
                .ReverseMap();
            
            CreateMap<Table, TableUpdateRequestDto>()
                .ReverseMap();
            
            CreateMap<Table, TableAddRequestDto>()
                .ReverseMap();

            CreateMap<ColumnPermission, ColumnPermissionResponseDto>();
            CreateMap<ColumnOperation, ColumnOperationResponseDto>();
            CreateMap<Column, ColumnResponseDto>();
            CreateMap<Table, TableResponseDto>();

            CreateMap<ColumnRequestDto, Column>();

            CreateMap<ColumnOperationResponseDto, ColumnOperation>();
            
            CreateMap<Table, TableNameResponseDto>();

            CreateMap<Column, ColumnResponseDto>()
                .ReverseMap();
            
            CreateMap<ColumnOperation, ViewColumnResponseDto>()
                .ForMember(dest => dest.Name, c => c.MapFrom(src => src.Column.Name))
                .ReverseMap();
            
            CreateMap<ColumnOperation, AddEditColumnResponseDto>()
                .ForMember(dest => dest.Name, c => c.MapFrom(src => src.Column.Name))
                .ForMember(dest => dest.DataType, c => c.MapFrom(src => src.Column.DataType))
                .ForMember(dest => dest.InputType, c => c.MapFrom(src => src.Column.InputType))
                .ForMember(dest => dest.InputResolverUrl, c => c.MapFrom(src => src.Column.InputResolverUrl))
                .ForMember(dest => dest.InputResolverList, c => c.MapFrom(src => src.Column.InputResolverList))
                .ForMember(dest => dest.Group1, c => c.MapFrom(src => src.Column.Group1))
                .ForMember(dest => dest.Group2, c => c.MapFrom(src => src.Column.Group2))
                .ReverseMap();

            CreateMap<FileInfo, FileInfoDto>()
                .ForMember(dest => dest.CreationDate, c => c.MapFrom(src => src.CreationTime))
                .ForMember(dest => dest.FilePath, c => c.MapFrom(src => src.FullName))
                .ForMember(dest => dest.LinesNumber, c => c.MapFrom(src => File.ReadAllLines(src.FullName).Length))
                .ForMember(dest => dest.Size, 
                    c => c.MapFrom(src => decimal.Round(src.Length / (decimal) 1048576, 2, MidpointRounding.AwayFromZero)));

            CreateMap<DataManagement, ImportResponseDto>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Table));
            
            CreateMap<DataManagement, SyncResponseDto>()
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => src.SyncUrl));

            CreateMap<Settings, SettingsDto>()
                .ReverseMap();

        }
    }
}