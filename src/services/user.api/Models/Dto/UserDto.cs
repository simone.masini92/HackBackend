using System;

namespace user.api.Models.Dto
{
    public class UserRequestDto
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public int RoleBackoffice { get; set; }
        public int RoleFrontoffice { get; set; }
    }
    
    public class UserAddRequestDto : UserRequestDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
    }

    public class UserResponseDto
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Fullname { get; set; }
        public string RoleBackoffice { get; set; }
        public string RoleFrontoffice { get; set; }
        public string SecurityLevel { get; set; }
    }
}