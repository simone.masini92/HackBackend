using System;
using System.ComponentModel.DataAnnotations;

namespace user.api.Models.Dto
{
    public class LoginRequestDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
    
    public class LoginResponseDto
    {
        public LoginResponseDto(Guid id, string email, string username, string token, byte[] image, bool temporary, bool changePassword)
        {
            Id = id;
            Email = email;
            Token = token;
            Username = username;
            Image = image;
            Temporary = temporary;
            ChangePassword = changePassword;
        }

        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string Username { get; set; }
        public byte[] Image { get; set; }
        public bool Temporary { get; set; }
        public bool ChangePassword { get; set; }
    }
}