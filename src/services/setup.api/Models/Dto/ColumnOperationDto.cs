using System.Collections.Generic;

namespace setup.api.Models.Dto
{
    public class ColumnOperationResponseDto
    {
        public string OperationType { get; set; }
        public bool? Hidden { get; set; }
        public bool? Avoid { get; set; }
        public List<ColumnPermissionResponseDto> ColumnPermissions { get; set; }
    }
}