using System;
using System.Collections.Generic;
using System.Linq;

namespace utilities.Class
{
    public class ReflectionUtility
    {
        public static Dictionary<string, string> GetPropertiesAndType<TSource>(List<TSource> materializedData)
        {
            var map = new Dictionary<string, string>();
            Type type;
            try
            {
                type = ((object)materializedData[0]).GetType();
            }
            catch (Exception)
            {
                return map;
            }
            var dynamicProperties = type.GetProperties();
            dynamicProperties.ToList().ForEach(property =>
            {
                var normalizedProperty = NormalizeProperty(property.PropertyType);
                map.Add(property.Name, normalizedProperty);
            });
            return map;
        }

        private static string NormalizeProperty(Type propertyType)
        {
            var type = "list";
            if (IsNullable(propertyType)) propertyType = Nullable.GetUnderlyingType(propertyType);

            if (propertyType == typeof(int) || propertyType == typeof(long)
                || propertyType == typeof(double) || propertyType == typeof(decimal))
            {
                type = "number";
            }
            
            if (propertyType == typeof(bool))
            {
                type = "boolean";
            }

            if (propertyType == typeof(string) || propertyType == typeof(Guid) || propertyType == typeof(char))
            {
                type = "string";
            }
            
            if (propertyType == typeof(DateTime))
            {
                type = "date";
            }

            return type;
        }

        private static bool IsNullable(Type type) => Nullable.GetUnderlyingType(type) != null;
    }
}