using Microsoft.AspNetCore.SignalR;

namespace infrastructure.Hubs
{
    public class SignalRAuthHub : Hub
    {
        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }
    }
}