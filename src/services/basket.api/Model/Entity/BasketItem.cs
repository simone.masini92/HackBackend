using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace basket.api.Model.Entity
{
    public class BasketItem
    {
        public Guid Id { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public decimal? Discount { get; set; }
        public int Order { get; set; }
        public int CustomerBasketCode { get; set; }
        
        public virtual CustomerBasket CustomerBasket { get; set; }
    }

    public class BasketItemConfiguration : IEntityTypeConfiguration<BasketItem>
    {
        public void Configure(EntityTypeBuilder<BasketItem> builder)
        {
            builder
                .ToTable("BasketItem");
            
            builder
                .HasKey(k => k.Id);

            builder
                .Property(c => c.Id)
                .ValueGeneratedOnAdd();

            builder
                .Property(c => c.Order)
                .UseSqlServerIdentityColumn()
                .Metadata.AfterSaveBehavior = PropertySaveBehavior.Ignore;
            
            builder
                .Property(ci => ci.Price)
                .HasColumnType("decimal(5, 2)");
            
            builder
                .Property(ci => ci.Discount)
                .HasColumnType("decimal(5, 2)");
        }
    }
}