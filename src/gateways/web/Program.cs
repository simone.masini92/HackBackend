﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            BuildWebHost(args)
                .Run();
        }

        private static IWebHost BuildWebHost(string[] args)
        {
            var builder = WebHost.CreateDefaultBuilder(args);

            builder.ConfigureServices(s => s.AddSingleton(builder))
                .ConfigureAppConfiguration((hostingContext, ic) =>
                {
                    ic.AddJsonFile("ocelot.json", false, true);
                    ic.AddJsonFile("appsettings.json", false, true);
                    ic.AddJsonFile($"ocelot.{hostingContext.HostingEnvironment.EnvironmentName}.json", false, true);
                    ic.AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", false, true);
                })
                .ConfigureLogging((hostingContext, build) =>
                {
                    build.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    build.AddConsole();
                    build.AddDebug();
                })
                .UseNLog()
                .UseStartup<Startup>();

            
            var host = builder.Build();
            return host;
        }
    }
}
