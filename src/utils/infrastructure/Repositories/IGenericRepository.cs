﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace infrastructure.Repositories
{
    public interface IGenericRepository<T, TContext>
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetAllNoTracking();
        IQueryable<T> GetAllWhere(Expression<Func<T, bool>> lambda);
        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> lambda);
        Task<T> GetByKeyAsync<TKey>(TKey key);
        Task<HttpStatusCode> UpdateAsync<TKey>(TKey key, T item);
        Task<T> AddAsync(T item);
        Task AddRangeAsync(IEnumerable<T> item);
        Task<IEnumerable<T>> DeleteByKeysAsync<TKey>(IEnumerable<TKey> keys);
        Task DeleteAllAsync();
        IQueryable<string> SelectWhere(Expression<Func<T, string>> lambda);
        Task DeleteWhereAsync(Expression<Func<T, bool>> lambda);
        Task<T> DeleteFirstOrDefaultAsync(Expression<Func<T, bool>> lambda);
        Task SaveChangesAsync();
        void Add(T item);
        void AddIfNotExists(T entity, Expression<Func<T, bool>> predicate);
        void Update(T item);
    }
}
