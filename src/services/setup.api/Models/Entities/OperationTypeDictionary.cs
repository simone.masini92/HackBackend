using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace setup.api.Models.Entities
{
    public class OperationTypeDictionary
    {
        public string OperationType { get; set; }
    }
    
    public class OperationTypeDictionaryConfiguration : IEntityTypeConfiguration<OperationTypeDictionary>
    {
        public void Configure(EntityTypeBuilder<OperationTypeDictionary> builder)
        {
            builder
                .ToTable("OperationTypeDictionary");

            builder
                .HasKey(c => c.OperationType);
        }
    }
}