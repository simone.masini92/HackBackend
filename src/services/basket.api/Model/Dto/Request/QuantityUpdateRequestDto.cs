using System;

namespace basket.api.Model.Dto.Request
{
    public class QuantityUpdateRequestDto
    {
        public Guid BasketItemId { get; set; }
        public int Quantity { get; set; }
    }
}