using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using user.api.Models.Entities;

namespace user.api.Models.Entities
{
    public class User : IdentityUser<Guid>
    {
        public Guid? RoleId { get; set; }
        public bool? Recruit { get; set; }
        public bool? Editable { get; set; }
        public byte[] Image { get; set; }
        public bool Deleted { get; set; }
        public bool Active { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UserModifyDate { get; set; }
        public DateTime? AdminModifyDate { get; set; }
        public DateTime? ActiveFrom { get; set; }
        public DateTime? ActiveTo { get; set; }
        public DateTime PasswordExpirationDate { get; set; }
        public DateTime? LastLogin { get; set; }
        public int PasswordHistoryLimit { get; set; }
        public bool ForceResetPassword { get; set; }
        public int LoginExpiration { get; set; }
        
        public virtual List<PasswordHistory> PasswordHistories { get; set; }
        public virtual List<UserClaim> Claims { get; set; }
        public virtual List<UserLogin> Logins { get; set; }
        public virtual List<UserToken> Tokens { get; set; }
        public virtual Role Role { get; set; }
    }
    
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");

            builder
                .Property(c => c.Id)
                .ValueGeneratedOnAdd();

            builder
                .Property(c => c.PasswordHistoryLimit)
                .HasDefaultValue(3);
            
            builder
                .Property(c => c.LoginExpiration)
                .HasDefaultValue(8);

            builder
                .Property(c => c.CreateDate)
                .HasDefaultValue(DateTime.Now);
            
            builder
                .Property(c => c.UserModifyDate)
                .HasDefaultValue(DateTime.Now);
            
            builder
                .Property(c => c.PasswordExpirationDate)
                .HasDefaultValue(DateTime.Now.AddMonths(3));
            
            builder
                .Property(c => c.Recruit)
                .HasDefaultValue(true);
            
            builder
                .Property(c => c.Editable)
                .HasDefaultValue(true);

            /***** RELATIONS *****/

            builder
                .HasMany(p => p.PasswordHistories)
                .WithOne(p => p.User)
                .HasForeignKey(p => p.UserId);

            builder
                .HasOne(e => e.Role)
                .WithMany(e => e.Users)
                .HasForeignKey(uc => uc.RoleId);
            
            builder
                .HasMany(e => e.Claims)
                .WithOne(e => e.User)
                .HasForeignKey(uc => uc.UserId)
                .IsRequired();

            builder
                .HasMany(e => e.Logins)
                .WithOne(e => e.User)
                .HasForeignKey(ul => ul.UserId)
                .IsRequired();

            builder
                .HasMany(e => e.Tokens)
                .WithOne(e => e.User)
                .HasForeignKey(ut => ut.UserId)
                .IsRequired();

        }
    }
}