using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using basket.api.Infrastructure;
using basket.api.Model.Dto.Request;
using basket.api.Model.Dto.Response;
using basket.api.Model.Entity;
using infrastructure.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace basket.api.Services
{
    public class BasketService
    {
        private readonly IGenericRepository<BasketItem, BasketContext> _basketItemGenericRepository;
        private readonly IGenericRepository<CustomerBasket, BasketContext> _customerBasketGenericRepository;
        private readonly IEmailSender _emailSender;
        private readonly IMapper _mapper;
        
        public BasketService(
            IGenericRepository<BasketItem, BasketContext> basketItemGenericRepository,
            IGenericRepository<CustomerBasket, BasketContext> customerBasketGenericRepository,
            IEmailSender emailSender,
            IMapper mapper)
        {
            _basketItemGenericRepository = basketItemGenericRepository;
            _customerBasketGenericRepository = customerBasketGenericRepository;
            _emailSender = emailSender;
            _mapper = mapper;
        }

        public async Task<CustomerBasketResponseDto> GetCustomerBasketByCustomerId(Guid buyerId)
        {
            return await GetBuyerBasket(buyerId);
        }

        public async Task<CustomerBasketResponseDto> UpdateProductQuantity(QuantityUpdateRequestDto quantity)
        {
            var basketItem = await _basketItemGenericRepository.FirstOrDefaultAsync(b => b.Id == quantity.BasketItemId);
            var buyerId = basketItem.CustomerBasket.CustomerId;
            basketItem.Quantity = quantity.Quantity;
            await _basketItemGenericRepository.UpdateAsync(quantity.BasketItemId, basketItem);
            return await GetBuyerBasket(buyerId);
        }

        public async Task<CustomerBasketResponseDto> DeleteBasketItem(Guid basketItemId)
        {
            var buyerId = await GetBuyerIdByBasketItemId(basketItemId);
            await _basketItemGenericRepository.DeleteWhereAsync(item => item.Id == basketItemId);
            return await GetBuyerBasket(buyerId);
        }

        public async Task<CustomerBasketResponseDto> GetCustomerBasketByBasketId(Guid customerBasketId)
        {
            var basket = await _customerBasketGenericRepository.FirstOrDefaultAsync(c => c.Id == customerBasketId);
            return _mapper.Map<CustomerBasketResponseDto>(basket);
        }
        
        /************ MIGLIORARE ***************/

        public async Task SendOrderEmail(EmailOrderRequestDto emailOrder, IHostingEnvironment hostingEnvironment)
        {
            var html = File.ReadAllText($"{hostingEnvironment.ContentRootPath}/ShipperOrder.html");
            html = html.Replace("{{firstName}}", emailOrder.FirstName);
            html = html.Replace("{{lastName}}", emailOrder.LastName);
            html = html.Replace("{{courier}}", emailOrder.Courier);
            html = html.Replace("{{paymentMethod}}", emailOrder.PaymentMethod);
            html = html.Replace("{{referent}}", emailOrder.Referent);
            html = html.Replace("{{street}}", emailOrder.Street);
            html = html.Replace("{{zipCode}}", emailOrder.ZipCode);
            html = html.Replace("{{city}}", emailOrder.City);
            html = html.Replace("{{state}}", emailOrder.State);
            html = html.Replace("{{country}}", emailOrder.Country);
            html = html.Replace("{{finalPrice}}", Math.Round(emailOrder.FinalPrice, 2).ToString(CultureInfo.InvariantCulture));
            const string endBasketString = "<!--ENDBASKET-->";
            var initBasket = html.IndexOf("<!--INITBASKET-->", StringComparison.Ordinal);
            var endBasket = html.IndexOf(endBasketString, StringComparison.Ordinal);
            var basketHtml = html.Substring(initBasket, endBasket - initBasket + endBasketString.Length);
            for (var i = 0; i < emailOrder.Items.Count - 1; i++)
            {
                html = html.Replace("{{productName}}", emailOrder.Items[i].ProductName);
                html = html.Replace("{{quantity}}", emailOrder.Items[i].Quantity.ToString());
                html = html.Replace("{{price}}", Math.Round(emailOrder.Items[i].Price, 2).ToString(CultureInfo.InvariantCulture));
                html = html.Insert(endBasket + endBasketString.Length, basketHtml);
            }
            html = html.Replace("{{productName}}", emailOrder.Items[emailOrder.Items.Count - 1].ProductName);
            html = html.Replace("{{quantity}}", emailOrder.Items[emailOrder.Items.Count - 1].Quantity.ToString());
            html = html.Replace("{{price}}", Math.Round(emailOrder.Items[emailOrder.Items.Count - 1].Price, 2).ToString(CultureInfo.InvariantCulture));
            await _emailSender.SendEmailAsync(emailOrder.EmailAddress, "SHIPPING SUMMARY", html);
            await _emailSender.SendEmailAsync("developer@componentsengine.com", "SHIPPING SUMMARY", html);
        }
        
        /*******************************************/
        
        private async Task<CustomerBasketResponseDto> GetBuyerBasket(Guid buyerId)
        {
            var basket = await _customerBasketGenericRepository.FirstOrDefaultAsync(b => b.CustomerId == buyerId);
            return _mapper.Map<CustomerBasketResponseDto>(basket);
        }

        private async Task<Guid> GetBuyerIdByBasketItemId(Guid basketItemId)
        {
            var basketItem = await _basketItemGenericRepository.FirstOrDefaultAsync(b => b.Id == basketItemId);
            return basketItem.CustomerBasket.CustomerId;
        }
    }
}