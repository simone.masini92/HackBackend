namespace models
{
    public class EmailSettings
    {
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public bool EnableSsl { get; set; }
        public string MailUser { get; set; }
        public string MailPassword { get; set; }
    }
}