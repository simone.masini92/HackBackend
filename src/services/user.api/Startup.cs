﻿using System;
using System.Reflection;
using AutoMapper;
using Hangfire;
using infrastructure.Hubs;
using Microsoft.AspNetCore.Authentication;
using models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using user.api.Infrastructure;
using user.api.Models.Entities;
using utilities.Extension;

namespace user.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            return services
                .AddCustomMvc()
                .AddCustomSignalR()
                .AddCustomDbContext<UserContext>(Configuration, typeof(Startup))
                .AddCustomCAP(Environment.Version.ToString(), Environment.MachineName, Environment.UserDomainName, Configuration)
                .AddCustomIdentity()
                .AddCustomAuthentication(Configuration)
                .AddCustomJwt(Configuration)
                .Configure<UserSettings>(Configuration)
                .Configure<EmailSettings>(Configuration.GetSection("EmailSettings"))
                .AddCustomOptions()
                .AddCustomIISOptions()
                .AddCustomLogging(Configuration)
                .AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies())
                .AddCustomRedirect(6001)
                .AddCustomApiVersioning()
                .AddCustomSwagger("User.API", AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml")
                .CustomStartup(Assembly.GetExecutingAssembly());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApiVersionDescriptionProvider provider)
        {
            var pathBase = Configuration["PATH_BASE"];

            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }

            if (env.IsDevelopment())
            {
                app.UseHttpsRedirection(); // TODO: toglierla da qui?
                app.UseDeveloperExceptionPage();
                app.UseSwagger()
                    .UseSwaggerUI(c =>
                    {
                        // Build a swagger endpoint for each discovered API version
                        foreach (var description in provider.ApiVersionDescriptions)  
                        {  
                            c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());  
                        }
                    });
            }
            else
            {
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseCors("CorsPolicy");
            app.UseMvc();
            app.UseSignalR(routes =>
            {
                routes.MapHub<SignalRAuthHub>("/signalR/auth");
            });

            app.Run(context => context.Response.WriteAsync("FlyUser!"));
        }
    }

    public static class Extensions
    {
        public static IServiceCollection AddCustomIdentity(this IServiceCollection services)
        {
            services.AddDefaultIdentity<User>()
                .AddEntityFrameworkStores<UserContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                //Password settings
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequiredLength = 8;
                
                //Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 20;
                options.Lockout.AllowedForNewUsers = true;
                
                //User settings
                options.User.RequireUniqueEmail = false;
                options.SignIn.RequireConfirmedEmail = true;
            });

            return services;
        }
    }
}
