﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace infrastructure.Repositories
{
    /// <summary>
    /// This generic repository class allows to access to db for the classic (CRUD) operation
    /// </summary>
    /// <typeparam name="T">The entity with which the generic repository and the dbset are instantiated</typeparam>
    /// <typeparam name="TContext"></typeparam>
    public class GenericRepository<T, TContext> : IGenericRepository<T, TContext>
        where T : class where TContext : DbContext
    {
        private readonly TContext _context;
        private readonly DbSet<T> _dbSet;

        public GenericRepository(TContext context)
        {
            _context = context; // Dependency injection of context
            _dbSet = context.Set<T>(); // Instance DbSet for the entity
        }

        /// <summary>
        /// A not materialized list of all items in database
        /// </summary>
        /// <returns>Returns all items stored in dbSet</returns>
        public IQueryable<T> GetAll()
        {
            return _dbSet.AsQueryable();
        }
        
        public IQueryable<T> GetAllNoTracking()
        {
            return _dbSet.AsQueryable().AsNoTracking();
        }

        public IQueryable<string> SelectWhere(Expression<Func<T, string>> lambda)
        {
            return _dbSet.Select(lambda);
        }

        /// <summary>
        /// A not materialized list of all items filter by ${lamda}
        /// </summary>
        /// <param name="lambda">The expressio who tell how filter data. Ex: c => c.IsActive</param>
        /// <returns>Returns the filtered items</returns>
        public IQueryable<T> GetAllWhere(Expression<Func<T, bool>> lambda)
        {
            return _dbSet.Where(lambda).AsQueryable();
        }
        
        /// <summary>
        /// The first item where ${lamda}
        /// </summary>
        /// <param name="lambda">The expressio who tell how filter data. Ex: c => c.IsActive</param>
        /// <returns>Returns the item</returns>
        public Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> lambda)
        {
            return GetAllWhere(lambda).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Find an item in the db with the specific primary key
        /// </summary>
        /// <param name="key">The Guid of the item to find</param>
        /// <returns>If the item was found returns it, else returns null</returns>
        public async Task<T> GetByKeyAsync<TKey>(TKey key)
        {
            var item = await _dbSet.FindAsync(key);

            return item;
        }

        /// <summary>
        /// Update an element store on the database passing the item id and the updated object
        /// </summary>
        /// <param name="key">The id of the item to update</param>
        /// <param name="item">The item to update</param>
        /// <returns>Returns the status code of the operation. NoContent 204 is ok</returns>
        public async Task<HttpStatusCode> UpdateAsync<TKey>(TKey key, T item)
        {
            _context.Entry(item).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_dbSet.Find(key) == null)
                {
                    return HttpStatusCode.NotFound;
                }

                throw;
            }

            return HttpStatusCode.NoContent;
        }

        /// <summary>
        /// Store a new item on the database
        /// </summary>
        /// <param name="item">The item to add in the db</param>
        /// <returns>No returns</returns>
        public async Task<T> AddAsync(T item)
        {
            _dbSet.Add(item);
            await _context.SaveChangesAsync();
            return item;
        }
        
        /// <summary>
        /// Store an item list into database
        /// </summary>
        /// <param name="item">The item list</param>
        /// <returns></returns>
        public async Task AddRangeAsync(IEnumerable<T> item)
        {
            _dbSet.AddRange(item);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Delete an item for the database
        /// </summary>
        /// <param name="keys">The list keys of the items to remove</param>
        /// <returns>Returns the item deleted</returns>
        public async Task<IEnumerable<T>> DeleteByKeysAsync<TKey>(IEnumerable<TKey> keys)
        {
            var deletedItems = new List<T>();
            foreach (var key in keys)
            {
                var item = await _dbSet.FindAsync(key);
                if (item == null) continue;
                deletedItems.Add(item);
                _dbSet.Remove(item);
            }

            await _context.SaveChangesAsync();

            return deletedItems;
        }

        public async Task DeleteWhereAsync(Expression<Func<T, bool>> lambda)
        {
            var elementToRemove = _dbSet.Where(lambda);
            _dbSet.RemoveRange(elementToRemove);
            await _context.SaveChangesAsync();
        }
        
        public async Task<T> DeleteFirstOrDefaultAsync(Expression<Func<T, bool>> lambda)
        {
            var elementToRemove = await _dbSet.FirstOrDefaultAsync(lambda);
            _dbSet.RemoveRange(elementToRemove);
            await _context.SaveChangesAsync();
            return elementToRemove;
        }

        public async Task DeleteAllAsync()
        {
            _context.RemoveRange(_dbSet);
            await _context.SaveChangesAsync();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Add(T item)
        {
            _dbSet.Add(item);
        }
        
        public void AddIfNotExists(T entity, Expression<Func<T, bool>> predicate)
        {
            var exists = predicate != null ? _dbSet.Any(predicate) : _dbSet.Any();
            if (!exists)
            {
                _dbSet.Add(entity);
            }
        }
        
        public void Update(T item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}