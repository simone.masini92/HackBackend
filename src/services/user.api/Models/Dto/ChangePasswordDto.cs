namespace user.api.Models.Dto
{
    public class ChangePasswordRequestDto
    {
        public string Username { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}