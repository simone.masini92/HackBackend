﻿using System;
using System.Threading.Tasks;
using basket.api.Model.Dto.Request;
using basket.api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace basket.api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/baskets")]
    [ApiController]
    [Authorize]
    public class BasketController : ControllerBase
    {
        private readonly BasketService _basketService;
        private readonly ILogger<BasketController> _logger;
        private readonly IHostingEnvironment _hostingEnvironment;
        
        public BasketController(BasketService basketService, ILogger<BasketController> logger, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _basketService = basketService;
            _logger = logger;
        }
        
        [HttpGet("customer-basket-by-customer-id/{customerId}")]
        public async Task<ActionResult> GetCustomerBasketByCustomerId(Guid customerId)
        {
            _logger.LogInformation("Getting basket...");
            var basket = await _basketService.GetCustomerBasketByCustomerId(customerId);
            return Ok(basket);
        }
        
        [HttpGet("customer-basket-by-basket-id/{customerBasketId}")]
        public async Task<ActionResult> GetCustomerBasketByBasketId(Guid customerBasketId)
        {
            _logger.LogInformation("Getting basket...");
            var basket = await _basketService.GetCustomerBasketByBasketId(customerBasketId);
            return Ok(basket);
        }

        [HttpPut("basket-item/{basketItemId}")]
        public async Task<ActionResult> UpdateProductQuantity(Guid basketItemId, [FromBody] QuantityUpdateRequestDto quantity)
        {
            if (basketItemId != quantity.BasketItemId)
            {
                return BadRequest();
            }

            var basket = await _basketService.UpdateProductQuantity(quantity);
            return Ok(basket);
        }

        [HttpPost("send-order-email")]
        public async Task<ActionResult> SendOrderEmail([FromBody] EmailOrderRequestDto emailOrder)
        {
            await _basketService.SendOrderEmail(emailOrder, _hostingEnvironment);
            return Ok();
        }

        [HttpDelete("basket-item/{basketItemId}")]
        public async Task<ActionResult> DeleteBasketItem(Guid basketItemId)
        {
            var basket = await _basketService.DeleteBasketItem(basketItemId);
            return Ok(basket);
        }
    }
}
