using System.Linq;
using AutoMapper;
using basket.api.Model.Dto.Response;
using basket.api.Model.Entity;

namespace basket.api.Model.Mapper
{
    public class BasketProfile : Profile
    {
        public BasketProfile()
        {
            CreateMap<BasketItem, BasketItemResponseDto>()
                .ForMember(dest => dest.FinalPrice, opt => opt.MapFrom<BasketItemResolver>());

            CreateMap<CustomerBasket, CustomerBasketResponseDto>()
                .ForMember(dest => dest.TotalPrice, opt => opt.MapFrom<CustomerBasketTotalPriceResolver>())
                .ForMember(dest => dest.FinalPrice, opt => opt.MapFrom<CustomerBasketFinalPriceResolver>())
                .ForMember(dest => dest.BasketItems, opt => opt.MapFrom(c => c.BasketItems.OrderBy(s => s.Order)));
        }
    }
}