using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace setup.api.Models.Entities
{
    public class InputTypeDictionary
    {
        public string InputType { get; set; }
    }
    
    public class InputTypeDictionaryConfiguration : IEntityTypeConfiguration<InputTypeDictionary>
    {
        public void Configure(EntityTypeBuilder<InputTypeDictionary> builder)
        {
            builder
                .ToTable("InputTypeDictionary");

            builder
                .HasKey(c => c.InputType);
        }
    }
}