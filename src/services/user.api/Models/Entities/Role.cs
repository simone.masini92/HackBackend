using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace user.api.Models.Entities
{
    public class Role
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
        public virtual List<User> Users { get; set; }
        public virtual List<RolePermission> RolePermissions { get; set; }
    }
    
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder
                .ToTable("Role");
            
            builder
                .HasKey(k => k.Id);
            
            builder
                .HasIndex(c => c.Name)
                .IsUnique();
            
            builder
                .Property(c => c.Id)
                .ValueGeneratedOnAdd();
            
            builder
                .HasMany(c => c.RolePermissions)
                .WithOne(c => c.Role)
                .HasForeignKey(c => c.RoleId);
        }
    }
}