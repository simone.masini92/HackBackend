using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using setup.api.Models.Entities;
using utilities.Class;
using utilities.Extension;

namespace setup.api.Infrastructure.Seed
{
    public class DataTypeSeed
    {
        public static IEnumerable<DataTypeDictionary> GetFromFile(string contentRootPath, ILogger<SetupContextSeed> logger)
        {
            const string fileName = "DataType";
            var txtFile = Path.Combine(contentRootPath, "Setup", $"{fileName}.txt");

            if (!File.Exists(txtFile))
            {
                throw new Exception($"{contentRootPath}/Setup/{fileName}.txt does not exist");
            }

            string[] txtHeaders;
            try
            {
                txtHeaders = File.ReadLines(txtFile).First().ToLower().Split('\t').Trim();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw new Exception(ex.Message);
            }

            return File.ReadAllLines(txtFile)
                .Skip(1) //skip header row
                .Select(row => Regex.Split(row, "\t"))
                .SelectTry(x => Create(x, txtHeaders))
                .OnCaughtException(ex => { logger.LogError(ex.Message); return null; })
                .Where(x => x != null);
        }
        
        private static DataTypeDictionary Create(IReadOnlyList<string> column, string[] headers)
        {
            if (column.Count != headers.Length)
            {
                throw new Exception($"Column count '{column.Count}' not the same as headers count'{headers.Length}'");
            }
            
            var it = column[Array.IndexOf(headers, "datatype")].Trim();

            var dataType = new DataTypeDictionary
            {
                DataType = it
            };

            return dataType;
        }
    }
}