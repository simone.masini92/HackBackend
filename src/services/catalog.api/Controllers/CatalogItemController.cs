﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using catalog.api.Models.Dto.Request;
using catalog.api.Models.Dto.Response;
using catalog.api.Models.Entities;
using catalog.api.Services;
using models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace catalog.api.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/catalogs")]
    [ApiController]
    [Authorize(Roles = "CATALOG_BACKOFFICE")]
    public class CatalogItemController : ControllerBase
    {
        private readonly CatalogItemService _catalogItemService;
        private readonly ILogger<CatalogItemController> _logger;
        
        public CatalogItemController(CatalogItemService catalogItemService, ILogger<CatalogItemController> logger)
        {
            _catalogItemService = catalogItemService;
            _logger = logger;
        }
        
    }
}
