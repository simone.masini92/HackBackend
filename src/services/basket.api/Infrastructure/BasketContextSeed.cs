using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Retry;

namespace basket.api.Infrastructure
{
    public class BasketContextSeed
    {
        public async Task SeedAsync(BasketContext context, IHostingEnvironment env, ILogger<BasketContextSeed> logger)
        {
            var policy = CreatePolicy(logger, nameof(BasketContextSeed));

#pragma warning disable 1998
            await policy.ExecuteAsync(async () =>
#pragma warning restore 1998
            {
                var contentRootPath = env.ContentRootPath;
                
                if (!context.CustomersBasket.Any())
                {
                    /*await context.CustomersBasket.AddRangeAsync(CustomerBasketSeed.GetFromFile(contentRootPath, logger));
                    await context.SaveChangesAsync();*/
                }

                if (!context.BasketItems.Any())
                {
                    /*await context.BasketItems.AddRangeAsync(BasketItemSeed.GetFromFile(contentRootPath, logger));
                    await context.SaveChangesAsync();*/
                }
            });
        }
        
        private static AsyncRetryPolicy CreatePolicy(ILogger logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>()
                .WaitAndRetryAsync(
                    retryCount: retries,
                    sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
                    onRetry: (exception, timeSpan, retry, ctx) =>
                    {
                        logger.LogTrace($"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
                    }
                );
        }
    }
}