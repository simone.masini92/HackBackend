using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using user.api.Infrastructure;
using user.api.Services;
using utilities.Class;

namespace user.api.Controllers
{
    [ApiVersion("1.0")] 
    [Route("/api/v{version:apiVersion}/users")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly UserContext _dbContext;
        private readonly ILogger<UserController> _logger;

        public UserController(UserService userService, ILogger<UserController> logger, UserContext dbContext)
        {
            _userService = userService;
            _logger = logger;
            _dbContext = dbContext;
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUser(Guid userId)
        {
            var user = await _userService.GetUser(userId);
            return Ok(user);
        }

        [HttpGet("has-hash/{userName}")]
        [AllowAnonymous]
        public async Task<IActionResult> HasHash(string userName)
        {
            var hasIt = await _userService.HasHash(userName);
            return Ok(hasIt);
        }
    }
    
}