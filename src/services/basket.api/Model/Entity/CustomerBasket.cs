using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace basket.api.Model.Entity
{
    public class CustomerBasket
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public int Code { get; set; }
        public decimal? Discount { get; set; }
        
        public virtual List<BasketItem> BasketItems { get; set; }
    }
    
    public class CustomerBasketConfiguration : IEntityTypeConfiguration<CustomerBasket>
    {
        public void Configure(EntityTypeBuilder<CustomerBasket> builder)
        {
            builder
                .ToTable("CustomerBasket");
            
            builder
                .HasKey(k => k.Id);

            builder
                .HasIndex(c => c.Code)
                .IsUnique();

            builder
                .Property(c => c.Id)
                .ValueGeneratedOnAdd();
            
            builder
                .Property(ci => ci.Discount)
                .HasColumnType("decimal(5, 2)");
            
            /* RELAZIONI */

            builder
                .HasMany(e => e.BasketItems)
                .WithOne(c => c.CustomerBasket)
                .HasForeignKey(c => c.CustomerBasketCode)
                .HasPrincipalKey(c => c.Code);
        }
    }
}