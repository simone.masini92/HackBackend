using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Castle.Core.Internal;
using infrastructure.Hubs;
using infrastructure.Repositories;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using models;
using setup.api.Infrastructure;
using setup.api.Models.Entities;

namespace setup.api.Services
{
    public class SyncService
    {
        private readonly ILogger<SyncService> _logger;
        private readonly IGenericRepository<DataManagement, SetupContext> _dataManagementRepository;
        private readonly IMapper _mapper;
        private readonly IHubContext<SignalRSyncHub> _hub;

        public SyncService(
            ILogger<SyncService> logger,
            IGenericRepository<DataManagement, SetupContext> dataManagementRepository,
            IMapper mapper,
            IHubContext<SignalRSyncHub> hub)
        {
            _logger = logger;
            _dataManagementRepository = dataManagementRepository;
            _mapper = mapper;
            _hub = hub;
        }
        
        public async Task SetSynchro(SyncMessageDto message)
        {
            var record = await _dataManagementRepository.GetByKeyAsync(message.Id);
            record.ToSync = message.SetTo;
            await _dataManagementRepository.UpdateAsync(message.Id, record);
#pragma warning disable 4014
            _hub.Clients.All.SendAsync("to-sync", message);
#pragma warning restore 4014
        }

        public async Task<List<SyncResponseDto>> GetSyncTables()
        {
            var tables = await _dataManagementRepository.GetAllWhere(c => !c.SyncUrl.IsNullOrEmpty()).ToListAsync();
            return _mapper.Map<List<SyncResponseDto>>(tables);
        }

        public async Task SetSyncing(SyncMessageDto message)
        {
            var record = await _dataManagementRepository.GetByKeyAsync(message.Id);
            record.Syncing = message.SetTo;
            await _dataManagementRepository.UpdateAsync(message.Id, record);
#pragma warning disable 4014
            _hub.Clients.All.SendAsync("syncing", message);
#pragma warning restore 4014
        }

        public void SyncPercentage(SignalRSyncPercentage message)
        {
            _hub.Clients.All.SendAsync("sync-percentage", message);
        }
    }
}