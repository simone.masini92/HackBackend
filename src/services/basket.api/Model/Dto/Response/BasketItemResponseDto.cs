using System;
using AutoMapper;
using basket.api.Model.Entity;

namespace basket.api.Model.Dto.Response
{
    public class BasketItemResponseDto
    {
        public Guid Id { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public decimal FinalPrice { get; set; }
        public int Quantity { get; set; }
        public decimal? Discount { get; set; }
    }
    
    public class BasketItemResolver : IValueResolver<BasketItem, BasketItemResponseDto, decimal>
    {
        public decimal Resolve(BasketItem source, BasketItemResponseDto destination, decimal destMember, ResolutionContext context)
        {
            if (source.Discount == null || source.Discount == 0)
            {
                return source.Price * source.Quantity;
            }

            return (100 - (decimal)source.Discount) / 100 * source.Price * source.Quantity;
        }
    }
}