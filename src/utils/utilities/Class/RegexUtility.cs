using System;
using System.Text.RegularExpressions;

namespace utilities.Class
{
    public class RegexUtility
    {
        public static void IsValidEmail(string email)
        {
            var isValid = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (!isValid)
            {
                throw new Exception("Email is not valid");
            }
        }
    }
}