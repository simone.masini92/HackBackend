namespace setup.api.Models.Dto
{
    public class SettingsDto
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}