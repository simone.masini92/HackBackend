using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using models;
using Microsoft.Extensions.Options;

namespace infrastructure.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailSettings _emailSettings;

        public EmailSender(IOptions<EmailSettings> emailSettings)
        {
            _emailSettings = emailSettings.Value;
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            return Execute(subject, htmlMessage,  email);
        }

        private Task Execute(string subject, string message, string email)
        {
            var client = new SmtpClient(_emailSettings.SmtpHost, _emailSettings.SmtpPort) {
                Credentials = new NetworkCredential(_emailSettings.MailUser, _emailSettings.MailPassword),
                EnableSsl = _emailSettings.EnableSsl
            };
            return client.SendMailAsync(
                new MailMessage("developer@componentsengine.com", email, subject, message) { IsBodyHtml = true }
            );
        }
        
        /*TODO: COSTRUIRE UN METODO CON la classe MailMessage*/    
        /*TODO: SCRIVERE L'OnPostAsync*/
    }

    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string htmlMessage);
    }
}