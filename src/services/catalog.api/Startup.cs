﻿using System;
using System.Reflection;
using AutoMapper;
using catalog.api.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using utilities.Extension;

namespace catalog.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }
        
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            return services
                .AddCustomMvc()
                .AddCustomDbContext<CatalogContext>(Configuration, typeof(Startup))
                .Configure<CatalogSettings>(Configuration)
                .AddCustomOptions()
                .AddCustomIISOptions()
                .AddCustomLogging(Configuration)
                .AddCustomAuthentication(Configuration)
                .AddCustomJwt(Configuration)
                .AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies())
                .AddCustomRedirect(443)
                .AddCustomApiVersioning()
                .AddCustomSwagger("Catalog.API", AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml")
                .CustomStartup(Assembly.GetExecutingAssembly());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApiVersionDescriptionProvider provider)
        {
            var pathBase = Configuration["PATH_BASE"];

            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger()
                    .UseSwaggerUI(c =>
                    {
                        // Build a swagger endpoint for each discovered API version
                        foreach (var description in provider.ApiVersionDescriptions)  
                        {  
                            c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());  
                        } 
                    });
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            // app.UseHttpsRedirection();
            app.UseMvc();
            
            app.Run(context => context.Response.WriteAsync("FlyCatalog!"));
        }
    }
}
