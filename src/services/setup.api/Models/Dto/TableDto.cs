using System;
using System.Collections.Generic;

namespace setup.api.Models.Dto
{
    public class TableDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
    
    public class TableAddRequestDto
    {
        public string Name { get; set; }
        public string Key { get; set; }
    }
    
    public class TableUpdateRequestDto : TableDto
    {
        public string Key { get; set; }
    }

    public class TableResponseDto : TableDto
    {
        public string Key { get; set; }
        public List<ColumnResponseDto> Columns { get; set; }
    }
    
    public class TableNameResponseDto : TableDto
    {
    }
}