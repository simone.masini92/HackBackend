using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace setup.api.Models.Entities
{
    public class ColumnPermission
    {
        public Guid Id { get; set; }
        public Guid ColumnId { get; set; }
        public string Permission { get; set; }
        public bool? Hidden { get; set; }
        
        public virtual ColumnOperation ColumnOperation { get; set; }
    }
    
    public class ColumnPermissionConfiguration : IEntityTypeConfiguration<ColumnPermission>
    {
        public void Configure(EntityTypeBuilder<ColumnPermission> builder)
        {
            builder
                .ToTable("ColumnPermission");

            builder
                .HasKey(t => t.Id);

            builder
                .Property(c => c.Id)
                .HasDefaultValueSql("NEWID()");
            
            builder
                .Property(c => c.Hidden)
                .HasDefaultValue(false);
        }
    }
}