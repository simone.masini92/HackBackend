using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace catalog.api.Models.Entities
{
    public class CatalogItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? Discount { get; set; }
    }

    public class CatalogItemConfiguration : IEntityTypeConfiguration<CatalogItem>
    {
        public void Configure(EntityTypeBuilder<CatalogItem> builder)
        {
            builder.ToTable("CatalogItem");

            builder.HasKey(ci => ci.Id);

            builder
                .Property(c => c.Id)
                .ValueGeneratedOnAdd();
            
            builder.Property(ci => ci.Price)
                .HasColumnType("decimal(5, 2)");
            
            builder.Property(ci => ci.Discount)
                .HasColumnType("decimal(5, 2)");
        }
    }
}