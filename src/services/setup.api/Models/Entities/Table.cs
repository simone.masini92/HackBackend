using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Policy;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace setup.api.Models.Entities
{
    public class Table
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }
        
        public virtual List<Column> Columns { get; set; }
    }

    public class TableConfiguration : IEntityTypeConfiguration<Table>
    {
        public void Configure(EntityTypeBuilder<Table> builder)
        {
            builder
                .ToTable("Table");

            builder
                .HasKey(c => c.Id);

            builder
                .HasIndex(c => c.Name)
                .IsUnique();

            builder
                .Property(c => c.Id)
                .HasDefaultValueSql("NEWID()");

            builder
                .Property(c => c.Key)
                .IsRequired();

            builder
                .HasMany(e => e.Columns)
                .WithOne(e => e.Table)
                .HasForeignKey(e => e.TableId);
        }
    }
}