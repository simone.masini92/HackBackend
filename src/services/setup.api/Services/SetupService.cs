using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Castle.Core.Internal;
using infrastructure.Repositories;
using Microsoft.AspNetCore.Identity.UI.V4.Pages.Internal.Account.Manage;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using models;
using Microsoft.Extensions.Logging;
using setup.api.Infrastructure;
using setup.api.Models.Dto;
using setup.api.Models.Entities;

namespace setup.api.Services
{
    public class SetupService
    {
        private readonly IMapper _mapper;
        private readonly ILogger<SetupService> _logger;
        private readonly IGenericRepository<Table, SetupContext> _tableRepository;
        private readonly IGenericRepository<DataTypeDictionary, SetupContext> _dataTypeRepository;
        private readonly IGenericRepository<InputTypeDictionary, SetupContext> _inputTypeRepository;
        private readonly IGenericRepository<OperationTypeDictionary, SetupContext> _operationTypeRepository;
        private readonly IGenericRepository<Column, SetupContext> _columnRepository;
        private readonly IGenericRepository<ColumnOperation, SetupContext> _columnOperationRepository;

        public SetupService(
            IMapper mapper,
            ILogger<SetupService> logger,
            IGenericRepository<Table, SetupContext> tableRepository,
            IGenericRepository<DataTypeDictionary, SetupContext> dataTypeRepository,
            IGenericRepository<InputTypeDictionary, SetupContext> inputTypeRepository,
            IGenericRepository<OperationTypeDictionary, SetupContext> operationTypeRepository,
            IGenericRepository<Column, SetupContext> columnRepository,
            IGenericRepository<ColumnOperation, SetupContext> columnOperationRepository)
        {
            _mapper = mapper;
            _logger = logger;
            _tableRepository = tableRepository;
            _dataTypeRepository = dataTypeRepository;
            _inputTypeRepository = inputTypeRepository;
            _operationTypeRepository = operationTypeRepository;
            _columnRepository = columnRepository;
            _columnOperationRepository = columnOperationRepository;
        }

        public async Task<List<TableNameResponseDto>> GetAllTables()
        {
            var tables = await _tableRepository
                .GetAll()
                .OrderBy(c => c.Name)
                .ToListAsync();
            return _mapper.Map<List<TableNameResponseDto>>(tables);
        }

        public async Task<TableResponseDto> GetTable(Guid tableId)
        {
            var table = await _tableRepository.FirstOrDefaultAsync(c => c.Id == tableId);
            table.Columns = table.Columns.OrderBy(c => c.Name).ToList();
            return _mapper.Map<TableResponseDto>(table);
        }

        public async Task<SetupResponseDto> GetSetup(string tableName)
        {
            var table = await _tableRepository.FirstOrDefaultAsync(c => c.Name == tableName);
            table.Columns.ForEach(column => column.ColumnOperations.RemoveAll(x => x.Avoid == true));
            table.Columns.RemoveAll(x => !x.ColumnOperations.Any());
            var recruitable = table.Columns.OrderBy(c => c.Sort ?? int.MaxValue).ToList();
            var tableMiddleResponse = GroupByOperation(recruitable);
            var addListGroup = GroupByGroup(tableMiddleResponse.AddColumns);
            var editListGroup = GroupByGroup(tableMiddleResponse.EditColumns);
            return new SetupResponseDto
            {
                AddColumns = addListGroup,
                EditColumns = editListGroup,
                ViewColumns = tableMiddleResponse.ViewColumns,
                PropertiesToSelect = tableMiddleResponse.ViewColumns.Select(c => c.Name).ToList(),
                Key = table.Key
            };
        }

        public async Task<HttpStatusCode> UpdateTable(Guid tableId, TableUpdateRequestDto tableUpdate)
        {
            var mappedTable = _mapper.Map<Table>(tableUpdate);
            return await _tableRepository.UpdateAsync(tableId, mappedTable);
        }

        public async Task<HttpStatusCode> UpdateColumn(Guid columnId, ColumnRequestDto columnUpdate)
        {
            if (columnUpdate.DataType.IsNullOrEmpty())
            {
                throw new Exception("DataType non può essere vuoto");
            }
            var oldColumn = await _columnRepository.GetByKeyAsync(columnId);
            oldColumn = _mapper.Map(columnUpdate, oldColumn);
            var mappedOp = _mapper.Map<List<ColumnOperation>>(columnUpdate.ColumnOperations);
            mappedOp.ForEach(x => x.ColumnId = columnId);
            oldColumn.ColumnOperations = mappedOp;
            return await _columnRepository.UpdateAsync(columnId, oldColumn);
        }
        /* TODO: habesum papam. Il mapping fatto qui sopra è da fare da tutte le parti */
        public async Task<Guid> AddTable(TableAddRequestDto table)
        {
            var mappedTable = _mapper.Map<Table>(table);
            var addedTable = await _tableRepository.AddAsync(mappedTable);
            return addedTable.Id;
        }

        public async Task<ColumnResponseDto> AddColumn(ColumnAddRequestDto item)
        {
            var column = new Column
            {
                Name = item.ColumnName,
                Nullable = false,
                ColumnOperations = new List<ColumnOperation>
                {
                    new ColumnOperation
                    {
                        Avoid = false,
                        Hidden = true,
                        OperationType = "ALL"
                    }
                },
                TableId = item.TableId
            };
            var addedColumn =  await _columnRepository.AddAsync(column);
            return _mapper.Map<ColumnResponseDto>(addedColumn);
        }

        public async Task<Guid> DeleteTable(Guid tableId)
        {
            var deletedItem = await _tableRepository.DeleteFirstOrDefaultAsync(c => c.Id == tableId);
            return deletedItem.Id;
        }

        public async Task<Guid> DeletedColumn(Guid columnId)
        {
            var deletedItem = await _columnRepository.DeleteFirstOrDefaultAsync(c => c.Id == columnId);
            return deletedItem.Id;
        }

        private SetupMiddleDto GroupByOperation(List<Column> recruitable)
        {
            var addList = new List<AddEditColumnResponseDto>();
            var editList = new List<AddEditColumnResponseDto>();
            var viewList = new List<ViewColumnResponseDto>();
            recruitable.ForEach(column =>
            {
                var nullable = column.Nullable;
                column.ColumnOperations.ForEach(operation =>
                {
                    switch (operation.OperationType)
                    {
                        case "ADD":
                        {
                            var temp = _mapper.Map<AddEditColumnResponseDto>(operation);
                            temp.Nullable = nullable;
                            addList.Add(temp);
                            break;
                        }
                        case "EDIT":
                        {
                            var temp = _mapper.Map<AddEditColumnResponseDto>(operation);
                            temp.Nullable = nullable;
                            editList.Add(temp);
                            break;
                        }
                        case "VIEW":
                        {
                            viewList.Add(_mapper.Map<ViewColumnResponseDto>(operation));
                            break;
                        }
                        case "ALL":
                        {
                            var temp = _mapper.Map<AddEditColumnResponseDto>(operation);
                            temp.Nullable = nullable;
                            addList.Add(temp);
                            editList.Add(temp);
                            viewList.Add(_mapper.Map<ViewColumnResponseDto>(operation));
                            break;
                        }
                    }
                });
            });
            return new SetupMiddleDto
            {
                AddColumns = addList,
                EditColumns = editList,
                ViewColumns = viewList
            };
        }

        private static dynamic GroupByGroup(List<AddEditColumnResponseDto> list)
        {
            if (list.Select(c => c.Group1).Any(i => i != null))
            {
                list.ForEach(column => {if (string.IsNullOrEmpty(column.Group1)) column.Group1 = "Default";});
                var groupedByFirst = list.GroupBy(x => x.Group1).ToDictionary(g => g.Key, g => g.ToList());
                var dictionaryToReturn = new Dictionary<string, dynamic>();
                foreach (var (key, value) in groupedByFirst)
                {
                    if (value.Select(q => q.Group2).Any(i => i != null))
                    {
                        value.ForEach(column => {if (string.IsNullOrEmpty(column.Group2)) column.Group2 = "Default";});
                        var groupedBySecond = value.GroupBy(g => g.Group2).ToDictionary(k => k.Key, k => k.ToList());
                        dictionaryToReturn.Add(key, groupedBySecond);
                    } else {dictionaryToReturn.Add(key, value);}
                }
                return dictionaryToReturn;
            }
            
            if (list.Select(q => q.Group2).Any(i => i != null))
            {
                list.ForEach(column => {if (string.IsNullOrEmpty(column.Group2)) column.Group2 = "Default";});
                var secondLevelGroup = list.GroupBy(d => d.Group2).ToDictionary(l => l.Key, l => l.ToList());
                return new Dictionary<string, Dictionary<string, List<AddEditColumnResponseDto>>>
                {
                    {"Default", secondLevelGroup}
                };
            }
            
            return new Dictionary<string, List<AddEditColumnResponseDto>>
            {
                {"Default", list}
            };
        }

        public async Task<SingleSetupResponseDto> GetSingleSetup(string tableName)
        {
            var table = await _tableRepository.FirstOrDefaultAsync(c => c.Name == tableName);
            var properties = SingleGetProperties(table.Columns.OrderBy(c => c.Sort ?? int.MaxValue).ToList());
            var setup = new SingleSetupResponseDto
            {
                Key = table.Key,
                Properties = properties,
                PropertiesToSelect = properties.Select(c => c.Name).ToList()
            };

            return setup;
        }

        /***************** DICTIONARIES ******************/

        public async Task<IEnumerable<string>> GetDataTypes()
        {
            return await _dataTypeRepository.GetAll().Select(c => c.DataType).ToListAsync();
        }

        public async Task<IEnumerable<string>> InputTypes()
        {
            return await _inputTypeRepository.GetAll().Select(c => c.InputType).ToListAsync();
        }

        public async Task<IEnumerable<string>> OperationTypes()
        {
            return await _operationTypeRepository.GetAll().Select(c => c.OperationType).ToListAsync();
        }
        
        /**************** PRIVATE **************************/
        
        private static List<SingleColumnDto> SingleGetProperties(List<Column> columns)
        {
            var columnList = new List<SingleColumnDto>();
            columns.ForEach(column =>
            {
                SingleColumnDto singleColumn;
                try
                {
                    singleColumn = new SingleColumnDto
                    {
                        Name = column.Name,
                        Editable = !GetHidden(column, "VIEW") && !GetHidden(column, "EDIT"),
                        Hidden = GetHidden(column, "VIEW"),
                        Nullable = column.Nullable,
                        DataType = column.DataType,
                        InputType = column.InputType,
                        InputResolverList = column.InputResolverList,
                        InputResolverUrl = column.InputResolverUrl
                    };
                }
                catch (Exception)
                {
                    throw new Exception($"{column.Name} operation of {column.Table.Name} not mapped");
                }

                if (!GetAvoid(column, "VIEW"))
                {
                    columnList.Add(singleColumn);
                }
            });
            return columnList;
        }

        private static bool GetHidden(Column column, string opType)
        {
            var operationType = column.ColumnOperations.SingleOrDefault(c => c.OperationType == "ALL" || c.OperationType == opType);
            if (operationType == null)
            {
                throw new Exception($"Property {opType} in {column.Name} with Id {column.Id} not found");
            }

            if (operationType.Hidden == null)
            {
                throw new Exception($"Hidden property in {column.Name} with Id {column.Id} is null");
            }
            
            return (bool) operationType.Hidden;
        }
        
        private static bool GetAvoid(Column column, string opType)
        {
            var operationType = column.ColumnOperations.SingleOrDefault(c => c.OperationType == "ALL" || c.OperationType == opType);
            if (operationType == null)
            {
                throw new Exception($"Property {opType} in {column.Name} with Id {column.Id} not found");
            }

            if (operationType.Avoid == null)
            {
                throw new Exception($"Avoid property in {column.Name} with Id {column.Id} is null");
            }
            
            return (bool) operationType.Avoid;
        }
    }
}