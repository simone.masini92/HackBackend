using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Castle.Core.Internal;
using infrastructure.Hubs;
using infrastructure.Repositories;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using models;
using setup.api.Infrastructure;
using setup.api.Models.Dto;
using setup.api.Models.Entities;
using utilities.Class;

namespace setup.api.Services
{
    public class ImportService
    {
        private readonly IMapper _mapper;
        private readonly ILogger<ImportService> _logger;
        private readonly IGenericRepository<Table, SetupContext> _tableRepository;
        private readonly IGenericRepository<DataManagement, SetupContext> _dataManageRepository;
        private readonly IHubContext<SignalRImportHub> _hub;

        public ImportService(
            IMapper mapper,
            ILogger<ImportService> logger,
            IGenericRepository<Table, SetupContext> tableRepository,
            IGenericRepository<DataManagement, SetupContext> dataManageRepository,
            IHubContext<SignalRImportHub> hub)
        {
            _mapper = mapper;
            _logger = logger;
            _tableRepository = tableRepository;
            _dataManageRepository = dataManageRepository;
            _hub = hub;
        }
        
        public async Task<List<ImportResponseDto>> GetAllTables()
        {
            var tables = await _dataManageRepository
                .GetAll()
                .OrderBy(c => c.Table)
                .ToListAsync();
            return _mapper.Map<List<ImportResponseDto>>(tables);
        }
        
        public async Task<List<string>> GetAllUrls()
        {
            return await _dataManageRepository.SelectWhere(c => c.ImportUrl).ToListAsync();
        }

        public List<FileInfoDto> ListFiles(string contentRootPath)
        {
            var filesInfo = new List<FileInfo>();
            var path = $"{contentRootPath}/SavedFiles";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var files = Directory.GetFiles(path).ToList();
            files.ForEach(file =>
            {
                filesInfo.Add(new FileInfo(file));
            });
            return _mapper.Map<List<FileInfoDto>>(filesInfo);
        }

        public async Task<ImportTablePropertiesResponseDto> TableImportProperties(Guid dataId)
        {
            var importTables = await _dataManageRepository.GetByKeyAsync(dataId);
            var pivots = importTables.Pivots;
            if (importTables.ImportUrl.IsNullOrEmpty())
            {
                throw new Exception("Per questa tabella non è stato specificato l'url su cui importare i dati");
            }
            var importTablesName = importTables.Table
                .Split(",")
                .Trim()
                .ToList();
            var tables = await _tableRepository.GetAll().ToListAsync();
            var propertiesList = new List<string>();
            importTablesName.ForEach(importTable =>
            {
                var table = tables.FirstOrDefault(c => c.Name == importTable);
                var properties = table?.Columns
                    .Where(c => c.ColumnOperations
                        .Any(a => (a.OperationType == "ADD" || a.OperationType == "ALL") && a.Hidden == false && a.Avoid == false))
                    .Select(c => c.Name)
                    .ToList();
                propertiesList.AddRange(properties);
            });
            

            return new ImportTablePropertiesResponseDto
            {
                Properties = propertiesList,
                ImportUrl = importTables.ImportUrl,
                Pivots = pivots,
                Operations = importTables.Operations
            };
        }

        public void ImportPercentage(SignalRImporting message)
        {
            _hub.Clients.Client(message.WebSocketConnectionId).SendAsync("import-percentage", message);
        }

        public void ImportResult(SignalRImportResult result)
        {
            _hub.Clients.Client(result.WebSocketConnectionId).SendAsync("import-result", result);
        }
    }
}