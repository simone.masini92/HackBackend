using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using basket.api.Model.Entity;

namespace basket.api.Model.Dto.Response
{
    public class CustomerBasketResponseDto
    {
        public Guid Id { get; set; }
        public Guid BuyerId { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal FinalPrice { get; set; }
        public decimal? Discount { get; set; }
        public List<BasketItemResponseDto> BasketItems { get; set; }
    }
    
    public class CustomerBasketTotalPriceResolver : IValueResolver<CustomerBasket, CustomerBasketResponseDto, decimal>
    {
        public decimal Resolve(CustomerBasket source, CustomerBasketResponseDto destination, decimal destMember, ResolutionContext context)
        {
            return destination.BasketItems.Sum(p => p.FinalPrice);
        }
    }
    
    public class CustomerBasketFinalPriceResolver : IValueResolver<CustomerBasket, CustomerBasketResponseDto, decimal>
    {
        public decimal Resolve(CustomerBasket source, CustomerBasketResponseDto destination, decimal destMember, ResolutionContext context)
        {
            if (source.Discount == null || source.Discount == 0)
            {
                return destination.TotalPrice;
            }

            return (100 - (decimal)source.Discount) / 100 * destination.TotalPrice;
        }
    }
}