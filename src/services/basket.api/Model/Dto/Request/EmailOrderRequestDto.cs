using System.Collections.Generic;

namespace basket.api.Model.Dto.Request
{
    public class EmailOrderRequestDto
    {
        public string EmailAddress { get; set; }
        public string Referent { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PaymentMethod { get; set; }
        public string Courier { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal FinalPrice { get; set; }
        public List<BasketItemForEmail> Items { get; set; }
    }

    public class BasketItemForEmail
    {
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}