﻿using System.Diagnostics;
using infrastructure.QueryInterceptor;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Web;
using user.api.Infrastructure;
using user.api.Models.Entities;
using utilities.Extension;

namespace user.api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            BuildWebHost(args)
                .MigrateDbContext<UserContext>((context, services) =>
                {
                    var env = services.GetService<IHostingEnvironment>();
                    var logger = services.GetService<ILogger<UserContextSeed>>();
                    
                    /****** PER IL SEED ******/
                    
                    var userManager = services.GetService<UserManager<User>>();
                    
                    /****** PER IL LOG DELLE QUERY ******/
                    
                    var loggerQueryInterceptor = services.GetService<ILogger<QueryInterceptor>>();
                    var listener = context.GetService<DiagnosticSource>();
                    (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor(loggerQueryInterceptor));
                    
                    /************************************/
                
                    new UserContextSeed(userManager)
                        .SeedAsync(context, env, logger)
                        .Wait();
                })
                .Run();
        }

        private static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    config.AddEnvironmentVariables();
                    config.AddJsonFile("appsettings.json", false, true);
                    config.AddJsonFile($"appsettings.{builderContext.HostingEnvironment.EnvironmentName}.json", false, true);
                })
                .ConfigureLogging((hostingContext, builder) =>
                {
                    builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    builder.AddConsole();
                    builder.AddDebug();
                })
                .UseNLog()
                .Build();
    }
}
