﻿using System;
using System.Reflection;
using AutoMapper;
using basket.api.Infrastructure;
using models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using utilities.Extension;

namespace basket.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            return services
                .AddCustomMvc()
                .AddCustomDbContext<BasketContext>(Configuration, typeof(Startup))
                .Configure<BasketSettings>(Configuration)
                .Configure<EmailSettings>(Configuration.GetSection("EmailSettings"))
                .AddCustomOptions()
                .AddCustomIISOptions()
                .AddCustomLogging(Configuration)
                .AddCustomAuthentication(Configuration)
                .AddCustomJwt(Configuration)
                .AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies())
                .AddCustomRedirect(7001)
                .AddCustomApiVersioning()
                .AddCustomSwagger("Basket.API", AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml")
                .CustomStartup(Assembly.GetExecutingAssembly());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApiVersionDescriptionProvider provider)
        {
            var pathBase = Configuration["PATH_BASE"];

            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger()
                    .UseSwaggerUI(c =>
                    {
                        // Build a swagger endpoint for each discovered API version
                        foreach (var description in provider.ApiVersionDescriptions)  
                        {  
                            c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());  
                        } 
                    });
            }
            else
            {
                app.UseHsts();
            }
            
            // app.UseHttpsRedirection();
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseMvc();
            
            app.Run(context => context.Response.WriteAsync("FlyBasket!"));
        }
    }
}
