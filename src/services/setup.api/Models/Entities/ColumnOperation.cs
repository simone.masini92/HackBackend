using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace setup.api.Models.Entities
{
    public class ColumnOperation
    {
        public Guid Id { get; set; }
        public Guid ColumnId { get; set; }
        public string OperationType { get; set; }
        public bool? Hidden { get; set; }
        public bool? Avoid { get; set; }
        
        public virtual List<ColumnPermission> ColumnPermissions { get; set; }
        public virtual Column Column { get; set; }
    }
    
    public class ColumnOperationConfiguration : IEntityTypeConfiguration<ColumnOperation>
    {
        public void Configure(EntityTypeBuilder<ColumnOperation> builder)
        {
            builder
                .ToTable("ColumnOperation");

            builder
                .HasKey(c => c.Id);
            
            builder
                .Property(c => c.Id)
                .HasDefaultValueSql("NEWID()");

            builder
                .HasIndex(t => new {t.OperationType, t.ColumnId})
                .IsUnique();

            builder.Property(c => c.OperationType)
                .IsRequired();
            
            builder
                .Property(c => c.Hidden)
                .HasDefaultValue(false);
            
            builder
                .Property(c => c.Avoid)
                .HasDefaultValue(false);

            builder
                .HasMany(c => c.ColumnPermissions)
                .WithOne(c => c.ColumnOperation)
                .HasForeignKey(c => c.ColumnId);
        }
    }
}