using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using setup.api.Models.Dto;
using setup.api.Services;

namespace setup.api.Controllers
{
    [ApiVersion("1.0")]
    [Route("/api/v{version:apiVersion}/setups")]
    [ApiController]
    [Authorize]
    public class SetupController : ControllerBase
    {
        private readonly SetupService _setupService;
        private readonly ILogger<SetupController> _logger;

        public SetupController(SetupService setupService, ILogger<SetupController> logger)
        {
            _setupService = setupService;
            _logger = logger;
        }
        
        [HttpGet("tables")]
        public async Task<IActionResult> GetAllTables()
        {
            var tables = await _setupService.GetAllTables();
            return Ok(tables);
        }
        
        [HttpGet("read/{tableId}")]
        public async Task<IActionResult> GetTable(Guid tableId)
        {
            var table = await _setupService.GetTable(tableId);
            return Ok(table);
        }

        [HttpGet("{tableName}")]
        public async Task<IActionResult> GetSetup(string tableName)
        {
            var setup = await _setupService.GetSetup(tableName);
            return Ok(setup);
        }
        
        [HttpGet("single/{tableName}")]
        public async Task<IActionResult> GetSingleSetup(string tableName)
        {
            var setup = await _setupService.GetSingleSetup(tableName);
            return Ok(setup);
        }
        
        [HttpPost("add-table")]
        public async Task<IActionResult> AddTable([FromBody] TableAddRequestDto table)
        {
            var newId = await _setupService.AddTable(table);
            return CreatedAtAction("AddTable", newId);
        }
        
        [HttpPost("add-column")]
        public async Task<IActionResult> AddColumn([FromBody] ColumnAddRequestDto column)
        {
            var newColumn = await _setupService.AddColumn(column);
            return CreatedAtAction("AddColumn", newColumn);
        }
        
        [HttpPut("{tableId}")]
        public async Task<IActionResult> UpdateTable([FromBody] TableUpdateRequestDto tableUpdate, Guid tableId)
        {
            if (tableId != tableUpdate.Id)
            {
                return BadRequest();
            }

            var status = await _setupService.UpdateTable(tableId, tableUpdate);

            return status.Equals(HttpStatusCode.NotFound)
                ? (IActionResult) NotFound()
                : NoContent();
        }
        
        [HttpPut("update-column/{columnId}")]
        public async Task<IActionResult> UpdateColumn([FromBody] ColumnRequestDto columnUpdate, Guid columnId)
        {
            if (columnId != columnUpdate.Id)
            {
                return BadRequest();
            }

            var status = await _setupService.UpdateColumn(columnId, columnUpdate);

            return status.Equals(HttpStatusCode.NotFound)
                ? (IActionResult) NotFound()
                : NoContent();
        }

        [HttpDelete("delete-table/{tableId}")]
        public async Task<IActionResult> DeleteTable(Guid tableId)
        {
            var deletedId = await _setupService.DeleteTable(tableId);
            return Ok(deletedId);
        }
        
        [HttpDelete("delete-column/{columnId}")]
        public async Task<IActionResult> DeleteColumn(Guid columnId)
        {
            var deletedId = await _setupService.DeletedColumn(columnId);
            return Ok(deletedId);
        }
        
        /***************** DICTIONARIES ******************/
        
        [HttpGet("data-types")]
        public async Task<IActionResult> GetDataTypes()
        {
            var data = await _setupService.GetDataTypes();
            return Ok(data);
        }
        
        [HttpGet("input-types")]
        public async Task<IActionResult> InputTypes()
        {
            var input = await _setupService.InputTypes();
            return Ok(input);
        }
        
        [HttpGet("operation-types")]
        public async Task<IActionResult> OperationTypes()
        {
            var op = await _setupService.OperationTypes();
            return Ok(op);
        }
    }
}