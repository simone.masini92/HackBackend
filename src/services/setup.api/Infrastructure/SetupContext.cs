﻿using Microsoft.EntityFrameworkCore;
using setup.api.Models.Entities;

namespace setup.api.Infrastructure
{
    public class SetupContext : DbContext
    {
        public SetupContext(DbContextOptions options) : base(options) { }
        
        //============= DbSet delle tabelle ===========//
        
        public DbSet<DataManagement> ImportsSetup { get; set; }
        public DbSet<Table> TablesSetup { get; set; }
        public DbSet<Column> ColumnsSetup { get; set; }
        public DbSet<ColumnOperation> ColumnsOperation { get; set; }
        public DbSet<ColumnPermission> ColumnPermissionsSetup { get; set; }
        public DbSet<DataTypeDictionary> DataTypesDictionary { get; set; }
        public DbSet<InputTypeDictionary> InputTypesDictionary { get; set; }
        public DbSet<OperationTypeDictionary> OperationTypesDictionary { get; set; }
        public DbSet<Settings> Configurations { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.ApplyConfiguration(new ImportConfiguration());
            modelBuilder.ApplyConfiguration(new TableConfiguration());
            modelBuilder.ApplyConfiguration(new ColumnConfiguration());
            modelBuilder.ApplyConfiguration(new ColumnOperationConfiguration());
            modelBuilder.ApplyConfiguration(new ColumnPermissionConfiguration());
            modelBuilder.ApplyConfiguration(new DataTypeDictionaryConfiguration());
            modelBuilder.ApplyConfiguration(new InputTypeDictionaryConfiguration());
            modelBuilder.ApplyConfiguration(new OperationTypeDictionaryConfiguration());
            modelBuilder.ApplyConfiguration(new SettingsConfiguration());
        }
    }
}
