﻿using System.Diagnostics;
using catalog.api.Infrastructure;
using infrastructure.QueryInterceptor;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using NLog.Web;
using utilities.Extension;

namespace catalog.api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            BuildWebHost(args)
                .MigrateDbContext<CatalogContext>((context, services) =>
                {
                    var env = services.GetService<IHostingEnvironment>();
                    var logger = services.GetService<ILogger<CatalogContextSeed>>();
                    
                    /****** PER IL LOG DELLE QUERY ******/
                    
                    var loggerQueryInterceptor = services.GetService<ILogger<QueryInterceptor>>();
                    var listener = context.GetService<DiagnosticSource>();
                    (listener as DiagnosticListener).SubscribeWithAdapter(new QueryInterceptor(loggerQueryInterceptor));
                    
                    /************************************/
                
                    new CatalogContextSeed()
                        .SeedAsync(context, env, logger)
                        .Wait();
                })
                .Run();
        }

        private static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    config.AddEnvironmentVariables();
                    config.AddJsonFile("appsettings.json", false, true);
                    config.AddJsonFile($"appsettings.{builderContext.HostingEnvironment.EnvironmentName}.json", false, true);
                })
                .ConfigureLogging((hostingContext, builder) =>
                {
                    builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    builder.AddConsole();
                    builder.AddDebug();
                })
                .UseNLog()
                .Build();
    }
}
