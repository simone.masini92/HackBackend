﻿using System;
using System.Reflection;
using AutoMapper;
using infrastructure.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using setup.api.Infrastructure;
using utilities.Extension;

namespace setup.api
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            return services
                .AddCustomMvc()
                .AddCustomSignalR()
                .AddCustomDbContext<SetupContext>(Configuration, typeof(Startup))
                .AddCustomCAP<SetupContext>(Environment.Version.ToString(), Environment.MachineName, Environment.UserDomainName, Configuration)
                .Configure<SetupSettings>(Configuration)
                .AddCustomOptions()
                .AddCustomIISOptions()
                .AddCustomLogging(Configuration)
                .AddCustomAuthentication(Configuration)
                .AddCustomJwt(Configuration)
                .AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies())
                .AddCustomRedirect(4001)
                .AddCustomApiVersioning()
                .AddCustomSwagger("Setup.API", AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml")
                .CustomStartup(Assembly.GetExecutingAssembly());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApiVersionDescriptionProvider provider)
        {
            var pathBase = Configuration["PATH_BASE"];
            
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }
            
            if (env.IsDevelopment())
            {
                app.UseHttpsRedirection(); // TODO: toglierla da qui?
                app.UseDeveloperExceptionPage();
                app.UseSwagger()
                    .UseSwaggerUI(c =>
                    {
                        // Build a swagger endpoint for each discovered API version
                        foreach (var description in provider.ApiVersionDescriptions)  
                        {  
                            c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());  
                        }
                    });
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseMvc();
            app.UseSignalR(routes =>
            {
                routes.MapHub<SignalRSyncHub>("/signalR/syncR");
                routes.MapHub<SignalRSettingsHub>("/signalR/settings");
                routes.MapHub<SignalRImportHub>("/signalR/importR");
            });
            
            app.Run(context => context.Response.WriteAsync("FlySetup!"));
        }
    }
}
