using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using user.api.Models.Dto;
using user.api.Models.Entities;
using user.api.Services;

namespace user.api.Controllers
{
    [ApiVersion("1.0")] 
    [Route("/api/v{version:apiVersion}/users/auth")]
    [ApiController]
    [Authorize]
    public class AuthController : ControllerBase
    {
        
        private readonly AuthService _authService;
        private readonly ILogger<AuthController> _logger;
        private readonly SignInManager<User> _signInManager;
        
        public AuthController(AuthService authService, ILogger<AuthController> logger, SignInManager<User> signInManager)
        {
            _authService = authService;
            _logger = logger;
            _signInManager = signInManager;
        }

        
        [HttpPost("sign-in")]
        [AllowAnonymous]
        public async Task<ActionResult<LoginResponseDto>> SignIn([FromBody] LoginRequestDto model)
        {
            _logger.LogInformation("Receiving sign in response...");
            var loginResponse = await _authService.SignIn(model);
            _logger.LogInformation("Sign in successful!");
            return Ok(loginResponse);
        }

        [HttpPost("sign-out")]
        [Authorize]
        public async Task<IActionResult> SignOut()
        {
            _logger.LogInformation("Signing out...");
            await _authService.SignOut();
            return Ok();
        }
        

        [HttpPost("sign-up")]
        [AllowAnonymous]
        public async Task<IActionResult> SignUp([FromBody] RegisterRequestDto model)
        {
            _logger.LogInformation("Signing up...");
            await _authService.SignUp(model);
            return Ok();
        }
        
        
        [HttpGet("confirm-email")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail([FromQuery(Name = "userId")] Guid userId, [FromQuery(Name = "code")] string code)
        {
            _logger.LogInformation("Confirm Email...");
            await _authService.ConfirmEmail(userId, code);
            return Ok();
        }
        
        
        [HttpPost("forgot-password")]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordRequestDto model)
        {
            _logger.LogInformation("Forgot password...");
            await _authService.ForgotPassword(model);
            return Ok();
        }
        
        
        [HttpPost("reset-password")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordRequestDto model)
        {
            _logger.LogInformation("Forgot password...");
            await _authService.ResetPassword(model);
            return Ok();
        }
        
        [HttpPost("change-password")]
        [AllowAnonymous]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordRequestDto passwords)
        {
            _logger.LogInformation("Change password...");
            await _authService.ChangePassword(passwords);
            return Ok();
        }
    }
}