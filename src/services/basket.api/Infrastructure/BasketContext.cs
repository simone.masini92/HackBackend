using basket.api.Model.Entity;
using Microsoft.EntityFrameworkCore;

namespace basket.api.Infrastructure
{
    public class BasketContext : DbContext
    {
        public BasketContext(DbContextOptions options) : base(options) { }
        
        //============= DbSet delle tabelle ===========//
        public DbSet<BasketItem> BasketItems { get; set; }
        public DbSet<CustomerBasket> CustomersBasket { get; set; }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.ApplyConfiguration(new BasketItemConfiguration());
            modelBuilder.ApplyConfiguration(new CustomerBasketConfiguration());
        }
    }
}